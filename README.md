# การเตรียมสภาพแวดล้อมของระบบ
1. ### ติดตั้ง npm และ yarn
1. ### ตรวจสอบ git, node.js, npm และ yarn
    ```
    git --version
    ```
    ```
    node -v
    ```
    ```
    npm -v
    ```
    ```
    yarn -v
    ```
# การติดตั้งและตั้งค่าระบบ
1. ### โคลนโปรเจ็ค
    ```
    git clone https://gitlab.com/tungman08/market-survey-vue.git
    ```
1. ### ติดตั้ง npm package
    ```
    cd ./market-survey-vue
    yarn install
    ```
1. ### สร้างไฟล์ .env
    ```
    cp .env.example .env
    ```
1. ### แก้ไขค่า config ใน .env
    - VUE_APP_NAME (ชื่อ app)
    - VUE_APP_API_URI (uri ของ api ตัวอย่าง http://localhost:3000)
1. ### เมื่อรันแล้ว สามารถเรียกใช้งานได้ ดังนี้ (ดูวิธีการรันใน development mode)
    - เรียกใช้งาน frontend ที่ url: http://localhost:8080
# การใช้งานสำหรับ development mode
1. ### คำสั่งในการทดสอบ
    ```
    yarn test:unit
    ```
1. ### คำสั่งในการรัน
    ```
    yarn serve
    ```
# การติดตั้งบน production
1. ### คำสั่งในการบิวด์ และ output ที่ได้จะถูกสร้างไว้ใน dist
    ```
    yarn build
    ```
1. ### ใช้ nginx เป็น web server เพื่อออก port: 80 โดยตั้งค่า root ไปที่ path ต้อง output ที่บิวด์ไว้ (ในตัวอย่างจะติดตั้งพร้อมทำ reverse proxy ตัว backend ซึ่งสมมุติว่ารัน service ที่ port:3000)
    <code>

        server {
            listen       80;

            location ^~ /api {
                rewrite           ^/api/(.*)$  /$1  break;
                proxy_set_header  X-Forwarded-For  $remote_addr;
                proxy_set_header  Host  $http_host;
                proxy_pass        http://localhost:3000;
            } 

            location / {
                root              ../../market-survey-vue/dist;
                try_files         $uri  $uri/  /index.html;
            }  
        }     
    </code>
1. ### สามารถเรียกผ่าน port: 80 ดังนี้
    - ตัว frontend -> http://\<IP Address>/
    - ตัว backend -> http://\<IP Address>/api/
    - ตัว api-docs -> http://\<IP Address>/api-docs/
