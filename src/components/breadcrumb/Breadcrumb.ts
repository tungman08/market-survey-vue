import { defineComponent, computed } from 'vue';
import { useRoute, useRouter } from 'vue-router';

interface Breadcrumb {
  display: string,
  link: string,
  active: boolean
}

export default defineComponent({
  name: 'Breadcrumb',
  setup() {
    const router = useRouter();
    const routes = router.getRoutes();

    const route = useRoute();
    const params = computed(() => getParams());
    const paths = computed(() => getPaths());
    const breadcrumbs = computed(() => getBreadcrumb());

    const getParams = () => {
      const paramArray: string[] = [];

      for (const key in route.params) {
        const param = route.params[key as keyof typeof route.params] as string;
        paramArray.push(param);
      }

      return paramArray;
    };

    const getPaths = () => {
      return [...new Set(route.path.split('/'))]
        .filter((path) => !params.value.includes(path))
        .reduce((pathArray: string[], path, index) => {
          pathArray.push(pathArray[index - 1]
              ? '/' + pathArray[index - 1] + '/' + path
              : '/' + path,
          );

          return pathArray;
        }, []);
    };

    const getBreadcrumb = () => {
      return paths.value.reduce((breadcrumb: Breadcrumb[], path) => {
        const rx = routes.find((r) => r.path === path);

        breadcrumb.push({
          // eslint-disable-next-line
          display: (path !== '/') ? `<span>${route.meta?.title}</span>` : `<span class="fa fa-home"></span>`,
          link: rx?.path as string,
          active: (!rx?.path) ? true : false
        });
        
        return breadcrumb;
      }, []);
    };

    return {
      breadcrumbs
    };
  }
});