import { defineComponent, computed, ref } from 'vue';
import { useRouter, useRoute } from 'vue-router';
import { useStore } from 'vuex';
import { Profile } from '../../store/models/profile.model';
import { useSweetAlert } from '../../compositions/sweetalert';
import { useToast } from 'vue-toastification';
import BreadcrumbComponent from '../breadcrumb/Breadcrumb.vue';
import ProfileComponent from '../profile/Profile.vue';
import ProfileFormComponent from '../profile-form/ProfileForm.vue';
import PasswordFormComponent from '../password-form/PasswordForm.vue';

export default defineComponent({
  name: 'Header',
  components: {
    BreadcrumbComponent,
    ProfileComponent,
    ProfileFormComponent,
    PasswordFormComponent
  },
  props: {
    profileLoaded: {
      type: Boolean,
      default: () => false
    }
  },
  setup(props) {
    const router = useRouter();
    const route = useRoute();
    const store = useStore();
    const swal = useSweetAlert();
    const toast = useToast();
    const profile = computed<Profile>(() => store.getters['profile/current']);
    const isSecretAdmin = computed<Profile>(() => store.getters['profile/isSecret']);
    const passwordChanged = computed<boolean>(() => !props.profileLoaded || profile.value?.passwordChanged);

    const profileModal = ref<InstanceType<typeof ProfileComponent>>();
    const profileFormModal = ref<InstanceType<typeof ProfileFormComponent>>();
    const passwordFormModal = ref<InstanceType<typeof PasswordFormComponent>>();
    const isHome = computed(() => route.path === '/');

    const profileClick = () => {
      profileModal.value?.showModal();
    };

    const profileFormOpen = () => {
      profileFormModal.value?.showModal();
    };

    const changePasswordClick = () => {
      if (isSecretAdmin.value) {
        swal.error('ไม่อนุญาต!', 'ขออภัย ท่านไม่สามารถเปลี่ยนรหัสผ่านของ Secret Admin ได้');
      } else {
        passwordFormModal.value?.showModal();
      }
    };

    const logoutClick = () => {
      swal.confirm('ออกจากระบบ!', 'คุณต้องการออกจากระบบใช่หรือไม่?', 'ใช่ ออกเดี๋ยวนี้!')
        .then((result) => {
          if (result.isConfirmed) {
            store.dispatch('auth/logout')
              .then(() => router.push('/auth/login'))
              .catch(() => toast.error('เกิดข้อผิดพลาด ไม่สามารถออกจากระบบได้'));
          }
      });
    };

    return {
      profile,
      profileModal,
      profileFormModal,
      passwordFormModal,
      passwordChanged,
      isHome,
      profileClick,
      profileFormOpen,
      changePasswordClick,
      logoutClick
    };
  }
});
