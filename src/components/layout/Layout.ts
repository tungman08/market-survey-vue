import { defineComponent, onMounted, ref, computed } from 'vue';
import { useRouter } from 'vue-router';
import { useStore } from 'vuex';
import HeaderComponent from '../header/Header.vue';
import LoadingComponent from '../loading/Loading.vue';

export default defineComponent({
  name: 'Layout',
  components: {
    HeaderComponent,
    LoadingComponent
  },
  setup() {
    const router = useRouter();
    const store = useStore();
    const isAuth = computed<boolean>(() => store.getters['auth/isAuth']);
    const isLoading = computed<boolean>(() => store.getters['profile/status'] === 'loading');
    const success = ref(false);

    onMounted(() => {
      if (!isAuth.value) {
        router.push('/auth/login');
      } else {
        store.dispatch('profile/get')
          .then(() => success.value = true)
          .catch(() => console.log('เกิดข้อผิดพลาดในการเรียก profile'));
      }
    });

    return {
      isLoading,
      success
    };
  }
});