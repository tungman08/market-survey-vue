import { defineComponent, onMounted, onUnmounted, ref } from 'vue';
import { Modal } from 'bootstrap';

export default defineComponent({
  name: 'Modal',
  emits: [
    'hide'
  ],
  props: {
    backdrop: {
      type: [String as () => 'static', Boolean],
      default: () => true
    },
    fullscreen: {
      type: Boolean,
      default: () => false
    },
    size: {
      type: [String as () => 'modal-sm' | 'modal-lg' | 'modal-xl']
    }
  },
  setup(props, context) {
    const modalElement = ref();
    const randomNumber = Math.floor(Math.random() * 1000000);
    const elementId = `modal_component_${randomNumber}`;

    onMounted(() => {
      const element = document.getElementById(elementId) as HTMLElement;
      modalElement.value = new Modal(element, {
        backdrop: props.backdrop
      });
    });

    onUnmounted(() => {
      modalElement.value?.dispose();
    });

    const showModal = () => {
      modalElement.value?.show();
    };

    const hideModal = async () => {
      await Promise.resolve(modalElement.value?.hide());
      context.emit('hide');
    };

    return {
      elementId,
      showModal,
      hideModal
    };
  }
});