import { defineComponent, ref, computed } from 'vue';

export default defineComponent({
  name: 'Paginate',
  emits: [
    'currentPageChange'
  ],
  props: {
    pageSize: {
      type: [Number, String],
      default: () => 10
    },
    amount: {
      type: [Number, String],
      required: true
    },
    total: {
      type: [Number, String],
      required: true
    }
  },
  setup(props, context) {
    const currentPage = ref(1);
    const maxPage = computed(() => Math.ceil(Number(props.amount) / Number(props.pageSize)));
    const pageList = computed(() => calulatePageList());

    const previousClick = async () => {
      await Promise.resolve(currentPage.value--);
      context.emit('currentPageChange', currentPage.value);
    };

    const nextClick = async () => {
      await Promise.resolve(currentPage.value++);
      context.emit('currentPageChange', currentPage.value);
    };

    const pageClick = async (page: number) => {
      await Promise.resolve(currentPage.value = page);
      context.emit('currentPageChange', currentPage.value);
    };

    const calulatePageList = () => {
      const pageArray = Array.from({length: maxPage.value}, (_, index) => index + 1);
      const start = (currentPage.value - 2 > 1)
        ? (maxPage.value - (currentPage.value - 2) > 5)
          ? currentPage.value - 2
          : maxPage.value - 4
        : 1;
      const end = (currentPage.value + 2 < maxPage.value)
        ? (currentPage.value + 2 > 5)
          ? currentPage.value + 2
          : 5
        : maxPage.value;

      return pageArray.slice(start - 1, end);
    };

    return {
      currentPage,
      maxPage,
      pageList,
      previousClick,
      nextClick,
      pageClick
    };
  }
});