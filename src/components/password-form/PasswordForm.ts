import { defineComponent, computed, ref, reactive } from 'vue';
import { useStore } from 'vuex';
import { useToast } from 'vue-toastification';
import { PasswordSchema } from '../../store/validators/profile.validator';
import ModalComponent from '../modal/Modal.vue';

export default defineComponent({
  name: 'PasswordForm',
  components: {
    ModalComponent
  },
  setup() {
    const store = useStore();
    const toast = useToast();
    const isLoading = computed<boolean>(() => store.getters['profile/status'] === 'loading');
    const passwordModal = ref<InstanceType<typeof ModalComponent>>();

    const formData = reactive({
      oldPassword: '',
      password: '',
      confirmPassword: ''
    });
    const validator = computed(() => PasswordSchema.validate(formData, { abortEarly: false }));
    const errors = reactive({
      oldPassword: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'oldPassword')),
      password: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'password')),
      confirmPassword: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'confirmPassword'))
    });
    const isValid = computed(() => !errors.oldPassword && !errors.password && !errors.confirmPassword);
    const submited = ref(false);

    const showModal = () => {
      passwordModal.value?.showModal();
    };

    const hideModal = () => {
      passwordModal.value?.hideModal();
    };

    const clearSubmit = () => {
      submited.value = false;
    };

    const clearForm = async () => {
      await Promise.all([
        formData.oldPassword = '',
        formData.password = '',
        formData.confirmPassword = ''
      ]);

      clearSubmit();
    };
    
    const submitClick = () => {
      submited.value = true;

      if (isValid.value) {
        store.dispatch('profile/changePassword', formData)
          .then(() => {
            toast.success('เปลี่ยนรหัสผ่านเรียบร้อยแล้ว');
            hideModal();
          })
          .catch(() => toast.error('ไม่สามารถเปลี่ยนรหัสผ่านได้'));
      }
    };

    return {
      passwordModal,
      formData,
      errors,
      submited,
      isLoading,
      showModal,
      hideModal,
      clearSubmit,
      clearForm,
      submitClick
    };
  }
});