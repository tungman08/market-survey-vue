import { defineComponent, ref } from 'vue';
import ModalComponent from '../modal/Modal.vue';

export default defineComponent({
  name: 'Preview',
  components: {
    ModalComponent
  },
  setup() {
    const previewModal = ref<InstanceType<typeof ModalComponent>>();

    const showModal = () => {
      previewModal.value?.showModal();
    };

    const hideModal = () => {
      previewModal.value?.hideModal();
    };

    return {
      previewModal,
      showModal,
      hideModal
    };
  }
});
