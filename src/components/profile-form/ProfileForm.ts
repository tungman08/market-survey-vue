import { defineComponent, ref, reactive, computed } from 'vue';
import { useStore } from 'vuex';
import { useToast } from 'vue-toastification';
import { Profile } from '../../store/models/profile.model';
import { ProfileSchema } from '../../store/validators/profile.validator';
import ModalComponent from '../modal/Modal.vue';

export default defineComponent({
  name: 'ProfileForm',
  components: {
    ModalComponent
  },
  setup() {
    const store = useStore();
    const toast = useToast();
    const profile = computed<Profile>(() => store.getters['profile/current']);
    const isLoading = computed<boolean>(() => store.getters['profile/status'] === 'loading');
    const profileFormModal = ref<InstanceType<typeof ModalComponent>>();

    const formData = reactive({
      firstName: '',
      lastName: '',
      email: ''
    });
    const validator = computed(() => ProfileSchema.validate(formData, { abortEarly: false }));
    const errors = reactive({
      firstName: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'firstName')),
      lastName: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'lastName')),
      email: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'email')),
    });
    const isValid = computed(() => !errors.firstName && !errors.lastName && !errors.email);
    const submited = ref(false);

    const showModal = async () => {
      await Promise.all([
        formData.firstName = profile.value.firstName,
        formData.lastName = profile.value.lastName,
        formData.email = profile.value.email
      ]);

      profileFormModal.value?.showModal(); 
    };

    const saveClick = () => {
      submited.value = true;

      if (isValid.value) {
        store.dispatch('profile/update', formData)
          .then(() => {
            toast.success('แก้ไขข้อมูลเรียบร้อยแล้ว');
            hideModal();
          })
          .catch(() => toast.error('ไม่สามารถแก้ไขข้อมูลได้'));
      }
    };

    const hideModal = () => {
      profileFormModal.value?.hideModal();
    };

    const clearSubmit = () => {
      submited.value = false;
    };

    const clearForm = async () => {
      await Promise.all([
          formData.firstName = '',
          formData.lastName = '',
          formData.email = ''
      ]);

      clearSubmit();
    };

    return {
      formData,
      errors,
      submited,
      profileFormModal,
      isLoading,
      showModal,
      hideModal,
      saveClick,
      clearSubmit,
      clearForm
    };
  }
});