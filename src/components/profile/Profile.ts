import { defineComponent, computed, ref } from 'vue';
import { useStore } from 'vuex';
import { Profile } from '../../store/models/profile.model';
import ModalComponent from '../modal/Modal.vue';

export default defineComponent({
  name: 'Profile',
  components: {
    ModalComponent
  },
  emits: [
    'edit'
  ],
  setup(_, context) {
    const store = useStore();
    const profile = computed<Profile>(() => store.getters['profile/current']);
    const profileModal = ref<InstanceType<typeof ModalComponent>>();

    const showModal = () => {
      profileModal.value?.showModal();
    };

    const hideModal = () => {
      profileModal.value?.hideModal();
    };

    const editClick = () => {
      hideModal();
      context.emit('edit');
    };

    return {
      profile,
      profileModal,
      showModal,
      hideModal,
      editClick
    };
  }
});