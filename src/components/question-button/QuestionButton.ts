import { defineComponent, ref, computed } from 'vue';
import { QuestionType } from '../../store/models/question.model';

export default defineComponent({
  name: 'QuestionButton',
  emits: [
    'selected'
  ],
  setup(_, context) {
    const properties = [
      { name: 'ข้อความ', icon: 'fa fa-font' },
      { name: 'ตัวเลือก', icon: 'far fa-dot-circle' },
      { name: 'คะแนน', icon: 'fa fa-star-half-alt' },
      { name: 'ลิเคิร์ท', icon: 'fa fa-th' },
      { name: 'วันเวลา', icon: 'far fa-calendar' },
    ];

    const questionTypes = computed(() => Object.keys(QuestionType)
      .map((typeName, index) => {
        return { 
          name: properties[index].name,
          icon: properties[index].icon,
          value: typeName.toLocaleLowerCase()
        };
      }));
    const toggle = ref(false);

    const toggleClick = () => {
      toggle.value = !toggle.value;
    };

    const selectedClick = (questionType: string) => {
      context.emit('selected', questionType);
      toggle.value = !toggle.value;
    };

    return {
      questionTypes,
      toggle,
      toggleClick,
      selectedClick
    };
  }
});
