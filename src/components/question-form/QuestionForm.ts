import { defineComponent, computed } from 'vue';
import { useStore } from 'vuex';

export default defineComponent({
  name: 'QuestonForm',
  props: {
    index: {
      type: Number,
      default: () => 0
    },
    isEditMode: {
      type: Boolean,
      default: () => false
    }
  },
  emits: [
    'save',
    'cancel',
    'remove',
    'moveUp',
    'moveDown'
  ],
  setup(_, context) {
    const store = useStore();
    const questionLength = computed<number>(() => (store.getters['questions/all']).length);
    const isLoading = computed<boolean>(() => store.getters['questions/status'] === 'loading');

    const saveClick = () => context.emit('save');
    const cancelClick = () => context.emit('cancel');
    const removeClick = () => context.emit('remove');
    const moveUpClick = () => context.emit('moveUp');
    const moveDownClick = () => context.emit('moveDown');

    return {
      isLoading,
      questionLength,
      saveClick,
      cancelClick,
      removeClick,
      moveUpClick,
      moveDownClick
    };
  }
});
