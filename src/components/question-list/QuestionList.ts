import { defineComponent, defineAsyncComponent, computed } from 'vue';
import { useStore } from 'vuex';
import { Question, QuestionType } from '../../store/models/question.model';

export default defineComponent({
  name: 'QuestionList',
  components: {
    AsyncTextboxComponent: defineAsyncComponent(() => import('../question/textbox/Textbox.vue')),
    AsyncChoiceComponent: defineAsyncComponent(() => import('../question/choice/Choice.vue')),
    AsyncLikertComponent: defineAsyncComponent(() => import('../question/likert/Likert.vue')),
    AsyncRatingComponent: defineAsyncComponent(() => import('../question/rating/Rating.vue')),
    AsyncDatetimeComponent: defineAsyncComponent(() => import('../question/datetime/Datetime.vue')),
  },
  setup() {
    const store = useStore();
    const questions = computed<Question[]>(() => store.getters['questions/all']);

    const questionComponent = (questionType: QuestionType) => {
      return `async-${questionType.toString().toLowerCase()}-component`;
    };

    return {
      questions,
      questionComponent
    };
  }
});
