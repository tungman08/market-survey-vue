import { defineComponent, defineAsyncComponent, reactive, computed } from 'vue';

export default defineComponent({
  name: 'QuestionNew',
  components: {
    AsyncQuestionButtonComponent: defineAsyncComponent(() => import('../question-button/QuestionButton.vue')),
    AsyncTextboxFormComponent: defineAsyncComponent(() => import('../question/textbox-form/TextboxForm.vue')),
    AsyncChoiceFormComponent: defineAsyncComponent(() => import('../question/choice-form/ChoiceForm.vue')),
    AsyncLikertFormComponent: defineAsyncComponent(() => import('../question/likert-form/LikertForm.vue')),
    AsyncRatingFormComponent: defineAsyncComponent(() => import('../question/rating-form/RatingForm.vue')),
    AsyncDatetimeFormComponent: defineAsyncComponent(() => import('../question/datetime-form/DatetimeForm.vue'))
  },
  setup() {
    const newQuestion = reactive({
      isAddMode: false,
      questionType: '',
    });
    const addQuestionComponent = computed(() => (newQuestion.isAddMode)
      ? `async-${newQuestion.questionType}-form-component`
      : 'async-question-button-component');

    const addQuestionClick = (questionType: string) => {
      newQuestion.isAddMode = true;
      newQuestion.questionType = questionType;
    };

    const toggleHandle = () => {
      clearAddMode();
    };

    const clearAddMode = () => {
      newQuestion.isAddMode = false;
      newQuestion.questionType = '';
    };

    return {
      addQuestionComponent,
      addQuestionClick,
      toggleHandle
    };
  }
});