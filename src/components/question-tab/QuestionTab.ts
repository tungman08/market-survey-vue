import { defineComponent } from 'vue';
import QuestionListComponent from '../question-list/QuestionList.vue';
import QuestionNewComponent from '../question-new/QuestionNew.vue';

export default defineComponent({
  name: 'QuestionTab',
  components: {
    QuestionListComponent,
    QuestionNewComponent
  }
});
