import { defineComponent, onMounted, PropType, ref, reactive, computed } from 'vue';
import { useStore } from 'vuex';
import { useSweetAlert } from '../../../compositions/sweetalert';
import { useToast } from 'vue-toastification';
import { Questionnaire } from '../../../store/models/questionnaire.model';
import { Question, QuestionType, QuestionChoice, QuestionOptionType } from '../../../store/models/question.model';
import { QuestionSchema } from '../../../store/validators/question.validator';
import QuestionFormComponent from '../../question-form/QuestionForm.vue';

export default defineComponent({
  name: 'ChoiceForm',
  components: {
    QuestionFormComponent
  },
  props: {
    index: {
      type: Number
    },
    question: {
      type: Object as PropType<Question>
    }
  },
  emits: [
    'toggle'
  ],
  setup(props, context) {
    const store = useStore();
    const swal = useSweetAlert();
    const toast = useToast();
    const questionnaire = computed<Questionnaire>(() => store.getters['questionnaire/selected']);
    const isEditMode = computed<boolean>(() => props.question !== undefined);

    // initial data
    const formData = reactive({
      id: '',
      questionnaire: questionnaire.value.id,
      required: false,
      questionType: QuestionType.Choice,
      questionBody: { 
        name: 'CHOICE',
        text: '',
        optionType: QuestionOptionType.DropDownList,
        options: ['ตัวเลือกที่ 1', 'ตัวเลือกที่ 2'],
        hasOther: false
      }
    });
    const validator = computed(() => QuestionSchema.validate(formData, { abortEarly: false }));
    const errors = reactive({
      questionBody: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'questionBody'))
    });
    const isValid = computed(() => !errors.questionBody);
    const submited = ref(false);

    const optionTypeName = ['ดรอปดาวน์ลิสต์', 'เลือกได้เพียงข้อเดียว', 'เลือกได้หลายข้อ'];
    const optionTypes = Object.values(QuestionOptionType).map((optionType, index) => {
      return {
        name: optionTypeName[index],
        value: optionType
      };
    });

    onMounted(() => {
      if (isEditMode.value) {
        formData.id = props.question?.id as string;
        formData.required = props.question?.required as boolean;

        const questionBody = props.question?.questionBody as QuestionChoice;
        formData.questionBody.text = questionBody.text;
        formData.questionBody.optionType = questionBody.optionType;
        formData.questionBody.options = questionBody.options.map((option) => option);
        formData.questionBody.hasOther = questionBody.hasOther;
      }
    });

    const dataChanged = () => {
      submited.value = false;
    };

    const saveHandle = () => {
      submited.value = true;

      if (isValid.value) {
        if (isEditMode.value) {
          update();
        } else {
          create();
        }
      } 
    };

    const cancelHandle = () => {
      clearForm();
      context.emit('toggle');
    };

    const removeHandle = () => {
      swal.confirm('ลบข้อคำถาม!', 'คุณต้องการลบข้อคำถามใช่หรือไม่?', 'ใช่ ลบออกเดี๋ยวนี้!')
        .then((result) => {
          if (result.isConfirmed) {
            store.dispatch('questions/remove', formData)
              .then(() => {
                toast.success('ลบข้อคำถามเรียบร้อยแล้ว');
                clearForm();
                context.emit('toggle');
              })
              .catch(() => toast.error('ไม่สามารถลบข้อคำถามได้'));
          }
        });
    };

    const moveUpHandle = () => {
      store.dispatch('questions/moveup', formData)
        .then(() => {
          toast.success('เลื่อนข้อคำถามขึ้นเรียบร้อยแล้ว');
          clearForm();
          context.emit('toggle');
        })
        .catch(() => toast.error('ไม่สามารถเลื่อนข้อคำถามได้'));
    };

    const moveDownHandle = () => {
      store.dispatch('questions/movedown', formData)
        .then(() => {
          toast.success('เลื่อนข้อคำถามลงเรียบร้อยแล้ว');
          clearForm();
          context.emit('toggle');
        })
        .catch(() => toast.error('ไม่สามารถเลื่อนข้อคำถามได้'));
    };

    const addOption = () => {
      const optionLength = formData.questionBody.options.length;
      formData.questionBody.options.push(`ตัวเลือกที่ ${optionLength + 1}`);
    };

    const removeOption = (index: number) => {
      formData.questionBody.options.splice(index, 1);
    };

    const create = () => {
      store.dispatch('questions/create', formData)
        .then(() => {
          toast.success('เพิ่มข้อคำถามเรียบร้อยแล้ว');
          clearForm();
          context.emit('toggle');
        })
        .catch(() => toast.error('ไม่สามารถเพิ่มข้อคำถามได้'));
    };

    const update = () => {
      store.dispatch('questions/update', formData)
        .then(() => {
          toast.success('แก้ไขข้อคำถามเรียบร้อยแล้ว');
          clearForm();
          context.emit('toggle');
        })
        .catch(() => toast.error('ไม่สามารถแก้ไขข้อคำถามได้'));
    };

    const clearForm = () => {
      formData.id = '';
      formData.required = false;
      formData.questionBody.text = '';
      formData.questionBody.optionType = QuestionOptionType.DropDownList;
      formData.questionBody.options = ['ตัวเลือกที่ 1', 'ตัวเลือกที่ 2'];
      formData.questionBody.hasOther = false;
    };

    return {
      formData,
      isEditMode,
      optionTypes,
      errors,
      submited,
      dataChanged,
      saveHandle,
      cancelHandle,
      removeHandle,
      moveUpHandle,
      moveDownHandle,
      addOption,
      removeOption
    };
  }
});
