import { defineComponent, defineAsyncComponent, PropType, ref, computed } from 'vue';
import { Question } from '../../../store/models/question.model';

export default defineComponent({
  name: 'Choice',
  components: {
    AsyncChoiceShowComponent: defineAsyncComponent(() => import('../choice-show/ChoiceShow.vue')),
    AsyncChoiceFormComponent: defineAsyncComponent(() => import('../choice-form/ChoiceForm.vue'))
  },
  props: {
    index: {
      type: Number,
      required: true
    },
    question: {
      type: Object as PropType<Question>,
      required: true
    }
  },
  setup() {
    const isEditMode = ref(false);
    const questionComponent = computed(() => (isEditMode.value)
      ? 'async-choice-form-component'
      : 'async-choice-show-component');

    const toggleHandle= () => {
      isEditMode.value = !isEditMode.value;
    };

    return {
      questionComponent,
      toggleHandle
    };
  }
});
