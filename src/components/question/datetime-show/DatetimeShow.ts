import { defineComponent, PropType } from 'vue';
import { Question } from '../../../store/models/question.model';

export default defineComponent({
  name: 'DatetimeShow',
  emits: [
    'toggle'
  ],
  props: {
    index: {
      type: Number,
      required: true
    },
    question: {
      type: Object as PropType<Question>,
      required: true
    }
  },
  setup(_, context) {
    const toggleClick = () => {
      context.emit('toggle');
    };

    return {
      toggleClick
    };
  }
});
