import { defineComponent, defineAsyncComponent, PropType, ref, computed } from 'vue';
import { Question } from '../../../store/models/question.model';

export default defineComponent({
  name: 'DateTime',
  components: {
    AsyncDatetimeShowComponent: defineAsyncComponent(() => import('../datetime-show/DatetimeShow.vue')),
    AsyncDatetimeFormComponent: defineAsyncComponent(() => import('../datetime-form/DatetimeForm.vue'))
  },
  props: {
    index: {
      type: Number,
      required: true
    },
    question: {
      type: Object as PropType<Question>,
      required: true
    }
  },
  setup() {
    const isEditMode = ref(false);
    const questionComponent = computed(() => (isEditMode.value)
      ? 'async-datetime-form-component'
      : 'async-datetime-show-component');

    const toggleHandle = () => {
      isEditMode.value = !isEditMode.value;
    };

    return {
      questionComponent,
      toggleHandle
    };
  }
});