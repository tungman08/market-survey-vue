import { defineComponent, onMounted, PropType, ref, reactive, computed } from 'vue';
import { useStore } from 'vuex';
import { useSweetAlert } from '../../../compositions/sweetalert';
import { useToast } from 'vue-toastification';
import { Questionnaire } from '../../../store/models/questionnaire.model';
import { Question, QuestionType, QuestionLikert } from '../../../store/models/question.model';
import { QuestionSchema } from '../../../store/validators/question.validator';
import QuestionFormComponent from '../../question-form/QuestionForm.vue';

export default defineComponent({
  name: 'LikertForm',
  components: {
    QuestionFormComponent
  },
  props: {
    index: {
      type: Number
    },
    question: {
      type: Object as PropType<Question>
    }
  },
  emits: [
    'toggle'
  ],
  setup(props, context) {
    const store = useStore();
    const swal = useSweetAlert();
    const toast = useToast();
    const questionnaire = computed<Questionnaire>(() => store.getters['questionnaire/selected']);
    const isEditMode = computed<boolean>(() => props.question !== undefined);

    // initial data
    const formData = reactive({
      id: '',
      questionnaire: questionnaire.value.id,
      required: false,
      questionType: QuestionType.Likert,
      questionBody: { 
        name: 'LIKERT',
        text: '',
        isCheckBox: false,
        rows: ['ตัววัดที่ 1', 'ตัววัดที่ 2'],
        columns: ['ตัวเลือกที่ 1', 'ตัวเลือกที่ 2', 'ตัวเลือกที่ 3']
      }
    });
    const validator = computed(() => QuestionSchema.validate(formData, { abortEarly: false }));
    const errors = reactive({
      questionBody: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'questionBody'))
    });
    const isValid = computed(() => !errors.questionBody);
    const submited = ref(false);

    const columnWidth = computed(() => {
      const freeSpace = 80;
      const percent = freeSpace / formData.questionBody.columns.length;
      return `width: ${percent}%;`;
    });

    onMounted(() => {
      if (isEditMode.value) {
        formData.id = props.question?.id as string;
        formData.required = props.question?.required as boolean;

        const questionBody = props.question?.questionBody as QuestionLikert;
        formData.questionBody.text = questionBody.text;
        formData.questionBody.isCheckBox = questionBody.isCheckBox;
        formData.questionBody.rows = questionBody.rows.map((row) => row);
        formData.questionBody.columns = questionBody.columns.map((column) => column);
      }
    });

    const dataChanged = () => {
      submited.value = false;
    };

    const saveHandle = () => {
      submited.value = true;
      
      if (isValid.value) {
        if (isEditMode.value) {
          update();
        } else {
          create();
        }
      } 
    };

    const cancelHandle = () => {
      clearForm();
      context.emit('toggle');
    };

    const removeHandle = () => {
      swal.confirm('ลบข้อคำถาม!', 'คุณต้องการลบข้อคำถามใช่หรือไม่?', 'ใช่ ลบออกเดี๋ยวนี้!')
        .then((result) => {
          if (result.isConfirmed) {
            store.dispatch('questions/remove', formData)
              .then(() => {
                toast.success('ลบข้อคำถามเรียบร้อยแล้ว');
                clearForm();
                context.emit('toggle');
              })
              .catch(() => toast.error('ไม่สามารถลบข้อคำถามได้'));
          }
        });
    };

    const moveUpHandle = () => {
      store.dispatch('questions/moveup', formData)
        .then(() => {
          toast.success('เลื่อนข้อคำถามขึ้นเรียบร้อยแล้ว');
          clearForm();
          context.emit('toggle');
        })
        .catch(() => toast.error('ไม่สามารถเลื่อนข้อคำถามได้'));
    };

    const moveDownHandle = () => {
      store.dispatch('questions/movedown', formData)
        .then(() => {
          toast.success('เลื่อนข้อคำถามลงเรียบร้อยแล้ว');
          clearForm();
          context.emit('toggle');
        })
        .catch(() => toast.error('ไม่สามารถเลื่อนข้อคำถามได้'));
    };

    const addRow = () => {
      const rowLength = formData.questionBody.rows.length;
      formData.questionBody.rows.push(`ตัววัดที่ ${rowLength + 1}`);
    };

    const removeRow = (index: number) => {
      formData.questionBody.rows.splice(index, 1);
    };

    const addColumn = () => {
      const columnLength = formData.questionBody.columns.length;
      formData.questionBody.columns.push(`ตัวเลือกที่ ${columnLength + 1}`);
    };

    const removeColumn = (index: number) => {
      formData.questionBody.columns.splice(index, 1);
    };

    const create = () => {
      store.dispatch('questions/create', formData)
        .then(() => {
          toast.success('เพิ่มข้อคำถามเรียบร้อยแล้ว');
          clearForm();
          context.emit('toggle');
        })
        .catch(() => toast.error('ไม่สามารถเพิ่มข้อคำถามได้'));
    };

    const update = () => {
      store.dispatch('questions/update', formData)
        .then(() => {
          toast.success('แก้ไขข้อคำถามเรียบร้อยแล้ว');
          clearForm();
          context.emit('toggle');
        })
        .catch(() => toast.error('ไม่สามารถแก้ไขข้อคำถามได้'));
    };

    const clearForm = () => {
      formData.id = '';
      formData.required = false;
      formData.questionBody.text = '';
      formData.questionBody.isCheckBox = false;
      formData.questionBody.rows = ['ตัววัดที่ 1', 'ตัววัดที่ 2'];
      formData.questionBody.columns = ['ตัวเลือกที่ 1', 'ตัวเลือกที่ 2', 'ตัวเลือกที่ 3'];
    };

    return {
      formData,
      isEditMode,
      columnWidth,
      errors,
      submited,
      dataChanged,
      saveHandle,
      cancelHandle,
      removeHandle,
      moveUpHandle,
      moveDownHandle,
      addRow,
      removeRow,
      addColumn,
      removeColumn
    };
  }
});
