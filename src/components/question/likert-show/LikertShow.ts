import { defineComponent, PropType, computed } from 'vue';
import { Question, QuestionLikert } from '../../../store/models/question.model';

export default defineComponent({
  name: 'LikertShow',
  emits: [
    'toggle'
  ],
  props: {
    index: {
      type: Number,
      required: true
    },
    question: {
      type: Object as PropType<Question>,
      required: true
    }
  },
  setup(props, context) {
    const questionBody = props.question.questionBody as QuestionLikert;
    const columnWidth = computed(() => {
      const freeSpace = 80;
      const percent = freeSpace / questionBody.columns.length;
      return `width: ${percent}%;`;
    });

    const toggleClick = () => {
      context.emit('toggle');
    };

    return {
      columnWidth,
      toggleClick
    };
  }
});
