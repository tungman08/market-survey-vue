import { defineComponent, defineAsyncComponent, PropType, ref, computed } from 'vue';
import { Question } from '../../../store/models/question.model';

export default defineComponent({
  name: 'Likert',
  components: {
    AsyncLikertShowComponent: defineAsyncComponent(() => import('../likert-show/LikertShow.vue')),
    AsyncLikertFormComponent: defineAsyncComponent(() => import('../likert-form/LikertForm.vue'))
  },
  props: {
    index: {
      type: Number,
      required: true
    },
    question: {
      type: Object as PropType<Question>,
      required: true
    }
  },
  setup() {
    const isEditMode = ref(false);
    const questionComponent = computed(() => (isEditMode.value)
      ? 'async-likert-form-component'
      : 'async-likert-show-component');

    const toggleHandle = () => {
      isEditMode.value = !isEditMode.value;
    };

    return {
      questionComponent,
      toggleHandle
    };
  }
});