import { defineComponent, onMounted, PropType, ref, reactive, computed } from 'vue';
import { useStore } from 'vuex';
import { useSweetAlert } from '../../../compositions/sweetalert';
import { useToast } from 'vue-toastification';
import { Questionnaire } from '../../../store/models/questionnaire.model';
import { Question, QuestionType, QuestionRating, QuestionRatingSymbol } from '../../../store/models/question.model';
import { QuestionSchema } from '../../../store/validators/question.validator';
import QuestionFormComponent from '../../question-form/QuestionForm.vue';
import RatingInputComponent from '../../rating-input/RatingInput.vue';

export default defineComponent({
  name: 'RatingForm',
  components: {
    QuestionFormComponent,
    RatingInputComponent
  },
  props: {
    index: {
      type: Number
    },
    question: {
      type: Object as PropType<Question>
    }
  },
  emits: [
    'toggle'
  ],
  setup(props, context) {
    const store = useStore();
    const swal = useSweetAlert();
    const toast = useToast();
    const questionnaire = computed<Questionnaire>(() => store.getters['questionnaire/selected']);
    const isEditMode = computed<boolean>(() => props.question !== undefined);

    // initial data
    const formData = reactive({
      id: '',
      questionnaire: questionnaire.value.id,
      required: false,
      questionType: QuestionType.Rating,
      questionBody: { 
        name: 'RATING',
        text: '',
        level: 5,
        symbol: QuestionRatingSymbol.Star,
        label: { 
          show: false,
          start: 'ต่ำสุด',
          end: 'สูงสุด'
        }
      }
    });
    const validator = computed(() => QuestionSchema.validate(formData, { abortEarly: false }));
    const errors = reactive({
      questionBody: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'questionBody'))
    });
    const isValid = computed(() => !errors.questionBody);
    const submited = ref(false);

    const symbolName = ['ดาว', 'ตัวเลข'];
    const symbols = Object.values(QuestionRatingSymbol).map((symbol, index) => {
      return {
        name: symbolName[index],
        value: symbol
      };
    });

    onMounted(() => {
      if (isEditMode.value) {
        formData.id = props.question?.id as string;
        formData.required = props.question?.required as boolean;

        const questionBody = props.question?.questionBody as QuestionRating;
        formData.questionBody.text = questionBody.text;
        formData.questionBody.level = questionBody.level;
        formData.questionBody.symbol = questionBody.symbol;
        formData.questionBody.label.show = questionBody.label.show;
        formData.questionBody.label.start = questionBody.label.start;
        formData.questionBody.label.end = questionBody.label.end;
      }
    });

    const dataChanged = () => {
      submited.value = false;
    };

    const saveHandle = () => {
      submited.value = true;

      if (isValid.value) {
        if (isEditMode.value) {
          update();
        } else {
          create();
        }
      } 
    };

    const cancelHandle = () => {
      clearForm();
      context.emit('toggle');
    };

    const removeHandle = () => {
      swal.confirm('ลบข้อคำถาม!', 'คุณต้องการลบข้อคำถามใช่หรือไม่?', 'ใช่ ลบออกเดี๋ยวนี้!')
        .then((result) => {
          if (result.isConfirmed) {
            store.dispatch('questions/remove', formData)
              .then(() => {
                toast.success('ลบข้อคำถามเรียบร้อยแล้ว');
                clearForm();
                context.emit('toggle');
              })
              .catch(() => toast.error('ไม่สามารถลบข้อคำถามได้'));
          }
        });
    };

    const moveUpHandle = () => {
      store.dispatch('questions/moveup', formData)
        .then(() => {
          toast.success('เลื่อนข้อคำถามขึ้นเรียบร้อยแล้ว');
          clearForm();
          context.emit('toggle');
        })
        .catch(() => toast.error('ไม่สามารถเลื่อนข้อคำถามได้'));
    };

    const moveDownHandle = () => {
      store.dispatch('questions/movedown', formData)
        .then(() => {
          toast.success('เลื่อนข้อคำถามลงเรียบร้อยแล้ว');
          clearForm();
          context.emit('toggle');
        })
        .catch(() => toast.error('ไม่สามารถเลื่อนข้อคำถามได้'));
    };

    const create = () => {
      store.dispatch('questions/create', formData)
        .then(() => {
          toast.success('เพิ่มข้อคำถามเรียบร้อยแล้ว');
          clearForm();
          context.emit('toggle');
        })
        .catch(() => toast.error('ไม่สามารถเพิ่มข้อคำถามได้'));
    };

    const update = () => {
      store.dispatch('questions/update', formData)
        .then(() => {
          toast.success('แก้ไขข้อคำถามเรียบร้อยแล้ว');
          clearForm();
          context.emit('toggle');
        })
        .catch(() => toast.error('ไม่สามารถแก้ไขข้อคำถามได้'));
    };

    const clearForm = () => {
      formData.id = '';
      formData.required = false;
      formData.questionBody.text = '';
      formData.questionBody.level = 5;
      formData.questionBody.symbol = QuestionRatingSymbol.Star;
      formData.questionBody.label.show = false;
      formData.questionBody.label.start = 'ต่ำสุด';
      formData.questionBody.label.end = 'สูงสุด';
    };

    return {
      formData,
      symbols,
      isEditMode,
      errors,
      submited,
      dataChanged,
      saveHandle,
      cancelHandle,
      removeHandle,
      moveUpHandle,
      moveDownHandle
    };
  }
});
