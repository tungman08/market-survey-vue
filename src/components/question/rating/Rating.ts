import { defineComponent, defineAsyncComponent, PropType, ref, computed } from 'vue';
import { Question } from '../../../store/models/question.model';

export default defineComponent({
  name: 'Rating',
  components: {
    AsyncRatingShowComponent: defineAsyncComponent(() => import('../rating-show/RatingShow.vue')),
    AsyncRatingFormComponent: defineAsyncComponent(() => import('../rating-form/RatingForm.vue'))
  },
  props: {
    index: {
      type: Number,
      required: true
    },
    question: {
      type: Object as PropType<Question>,
      required: true
    }
  },
  setup() {
    const isEditMode = ref(false);
    const questionComponent = computed(() => (isEditMode.value)
      ? 'async-rating-form-component'
      : 'async-rating-show-component');

    const toggleHandle = () => {
      isEditMode.value = !isEditMode.value;
    };

    return {
      questionComponent,
      toggleHandle
    };
  }
});