import { defineComponent, defineAsyncComponent, PropType, ref, computed } from 'vue';
import { Question } from '../../../store/models/question.model';

export default defineComponent({
  name: 'Textbox',
  components: {
    AsyncTextboxShowComponent: defineAsyncComponent(() => import('../textbox-show/TextboxShow.vue')),
    AsyncTextboxFormComponent: defineAsyncComponent(() => import('../textbox-form/TextboxForm.vue'))
  },
  props: {
    index: {
      type: Number,
      required: true
    },
    question: {
      type: Object as PropType<Question>,
      required: true
    }
  },
  setup() {
    const isEditMode = ref(false);
    const questionComponent = computed(() => (isEditMode.value)
      ? 'async-textbox-form-component'
      : 'async-textbox-show-component');

    const toggleHandle = () => {
      isEditMode.value = !isEditMode.value;
    };

    return {
      questionComponent,
      toggleHandle
    };
  }
});