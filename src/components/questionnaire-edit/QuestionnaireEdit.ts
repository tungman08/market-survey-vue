import { defineComponent, ref, reactive, computed } from 'vue';
import { useStore } from 'vuex';
import { useToast } from 'vue-toastification';
import { Questionnaire } from '../../store/models/questionnaire.model';
import QuestionnaireForm from '../../components/questionnaire-form/QuestionnaireForm.vue';
import { QuestionnaireSchema } from '../../store/validators/questionnaire.validtor';

export default defineComponent({
  name: 'QuestionnaireEdit',
  emits: [
    'toggle'
  ],
  components: {
    QuestionnaireForm
  },
  setup(_, context) {
    const store = useStore();
    const toast = useToast();
    const questionnaire = computed<Questionnaire>(() => store.getters['questionnaire/selected']);
    const isLoading = computed<boolean>(() => store.getters['questionnaire/status'] === 'loading');

    const formData = reactive({
      id: questionnaire.value.id,
      title: questionnaire.value.title,
      description: questionnaire.value.description,
      releaseAt: questionnaire.value.releaseAt,
      dueAt: questionnaire.value.dueAt
    });
    const validator = computed(() => QuestionnaireSchema.validate(formData, { abortEarly: false }));
    const errors = reactive({
      title: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'title')),
      description: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'description')),
      releaseAt: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'releaseAt')),
      dueAt: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'dueAt'))
    });
    const isValid = computed(() => !errors.title && !errors.description && !errors.releaseAt && !errors.dueAt);
    const submited = ref(false);

    const saveClick = () => {
      submited.value = true;

      if (isValid.value) {
        store.dispatch('questionnaire/update', formData)
          .then(() => {
            clearForm();
            toast.success('แก้ไขแบบสอบถามเรียบร้อยแล้ว');
          })
          .catch(() => toast.error('ไม่สามารถแก้ไขแบบสอบถามได้'));
      } 
    };

    const cancelClick = () => {
      clearForm();
      context.emit('toggle');
    };

    const clearSubmit = () => {
      submited.value = false;
    };

    const clearForm = async () => {
      await Promise.all([
        formData.title = '',
        formData.description = '',
        formData.releaseAt = new Date,
        formData.dueAt = new Date
      ]);

      clearSubmit();
    };

    return {
      isLoading,
      formData,
      errors,
      submited,
      saveClick,
      cancelClick,
      clearSubmit
    };
  }
});
