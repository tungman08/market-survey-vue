import { defineComponent, PropType } from 'vue';

export default defineComponent({
  name: 'QuestionnaireForm',
  props: {
    formData: {
      type: Object as PropType<{ 
        title: string,
        desscription: string,
        releaseAt: { type: Date, required: false },
        dueAt: { type: Date, required: false }
      }>,
			required: true
    },
    errors: {
      type: Object as PropType<{
        title: boolean,
        desscription: boolean,
        releaseAt: boolean,
        dueAt: boolean      
      }>,
			required: true
    },
    submited: {
      type: Boolean,
      required: true
    }
  },
  emits: [
    'dataChanged'
  ],
  setup(_, context) {
    const dataChanged = () => {
      context.emit('dataChanged');
    };

    return {
      dataChanged
    };
  }
});