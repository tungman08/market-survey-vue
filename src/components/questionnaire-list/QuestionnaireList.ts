import { defineComponent, reactive, computed } from 'vue';
import { useRouter } from 'vue-router';
import { useStore } from 'vuex';
import { useDateFormat } from '../../compositions/dateformat';
import { useSweetAlert } from '../../compositions/sweetalert';
import { useToast } from 'vue-toastification';
import { Questionnaire } from '../../store/models/questionnaire.model';
import PaginateComponent from '../paginate/Paginate.vue';

export default defineComponent({
  name: 'QuestionnaireList',
  components: {
    PaginateComponent
  },
  setup() {
    const router = useRouter();
    const store = useStore();
    const dateFormat = useDateFormat();
    const swal = useSweetAlert();
    const toast = useToast();
    const questionnaires = computed<Questionnaire[]>(() => store.getters['questionnaires/all']);
    const search = computed<Questionnaire[]>(() => questionnaires.value.filter((form) => toSearch(form)));
    const display = computed(() => search.value.filter((_, index) => toDisplay(index))
      .map((form) => toFormat(form)));

    const category = ['ทั้งหมด', 'ยังไม่เผยแพร่', 'เปิดรับความคิดเห็น', 'สิ้นสุดแล้ว'];
    const filter = reactive({
      category: category[0],
      keyword: '',
      amount: computed(() => search.value.length),
      total: computed(() => questionnaires.value.length)
    });

    const page = reactive({
      size: 8,
      current: 1
    });

    const selectedForm = (id: string) => {
      router.push(`/questionnaire/${id}`);
    };

    const selectedPage = (pageNumber: number) => {
      page.current = pageNumber;
    };

    const newBrowserClick = (id: string) => {
      const route = router.resolve(`/questionnaire/${id}`);
      window.open(route.href, '_blank');
    };

    const deleteClick = (id: string) => {
      swal.confirm('ลบแบบสอบถาม!', 'คุณต้องการลบแบบสอบถามใช่หรือไม่?', 'ใช่ ลบออกเดี๋ยวนี้!')
        .then((result) => {
          if (result.isConfirmed) {
            store.dispatch('questionnaires/remove', id)
              .then(() => toast.success('ลบแบบสอบถามเรียบร้อยแล้ว'))
              .catch(() => toast.error('ไม่สามารถลบแบบสอบถามได้'));
          }
        });
    };

    const toSearch = (form: Questionnaire): Questionnaire | boolean => {
      const today = new Date();
      const releaseAt = new Date(form.releaseAt);
      const dueAt = new Date(form.dueAt);
      const status = (filter.category !== 'ทั้งหมด')
        ? (releaseAt <= today)
          ? (dueAt < today)
            ? 'สิ้นสุดแล้ว'
            : 'เปิดรับความคิดเห็น'
          : 'ยังไม่เผยแพร่'
        : 'ทั้งหมด';

      return (filter.category === status)
        ? (filter.keyword)
          ? form.title.toUpperCase().includes(filter.keyword.toUpperCase())
            : form
            : false;
    };

    const toDisplay = (index: number) => {
      const start = (page.current - 1) * page.size;
      const end = start + page.size;
      return index >= start && index < end;
    };

    const toFormat = (form: Questionnaire) => {
      return {
        id: form.id,
        title: form.title,
        description: form.description,
        releaseAt: dateFormat.toThaiDate(form.releaseAt, 'short'),
        dueAt: dateFormat.toThaiDate(form.dueAt, 'short'),
        status: formStatus(form.releaseAt, form.dueAt)
      };
    };

    const formStatus = (releaseAt: string | Date, dueAt: string | Date) => {
      const today = new Date();
      const start = (typeof releaseAt === 'string') ? new Date(releaseAt) : releaseAt;
      const end = (typeof dueAt === 'string') ? new Date(dueAt) : dueAt;
      const bgColor = 'background-color: #ffc107;';
      const boxShadow = 'box-shadow: rgba(255, 193, 7, 0.95) 0px 0px 5px;';

      return (start <= today)
        ? (end < today)
          ? `<span class="text-success p-1" style="${bgColor} ${boxShadow}">การตอบกลับ: 0</span>`
          : `<span class="text-primary p-1" style="${bgColor} ${boxShadow}">กำลังเปิดรับความคิดเห็น</span>`
          : `<span class="p-1" style="${bgColor} ${boxShadow}">ยังไม่เผยแพร่</span>`;
    };

    // เลือก background โดยรวมผลรวมของ id จนเหลือเลขตัวเดียว
    const formImageBg = (id: string) => {
      const firstSum = [...id].map((str: string) => parseInt(str, 16))
        .reduce((sum: number, val: number) => sum + val);

      let result = firstSum.toString();
      while (result.length > 1) {
        const loopSum = [...result].map((str: string) => Number(str))
          .reduce((sum: number, val: number) => sum + val);

        result = loopSum.toString();
      }

      return `form-image-bg-${result}`;
    };

    return {
      category,
      display,
      filter,
      page,
      formImageBg,
      selectedForm,
      selectedPage,
      newBrowserClick,
      deleteClick
    };
  }
});