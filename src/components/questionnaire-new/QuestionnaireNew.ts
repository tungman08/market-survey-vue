import { defineComponent, ref, reactive, computed } from 'vue';
import { useStore } from 'vuex';
import { useToast } from 'vue-toastification';
import { QuestionnaireSchema } from '../../store/validators/questionnaire.validtor';
import QuestionnaireForm from '../../components/questionnaire-form/QuestionnaireForm.vue';
import ModalComponent from '../modal/Modal.vue';

export default defineComponent({
  name: 'QuestionnaireNew',
  components: {
    ModalComponent,
    QuestionnaireForm
  },
  setup() {
    const store = useStore();
    const toast = useToast();
    const isLoading = computed<boolean>(() => store.getters['questionnaires/status'] === 'loading');
    const formModal = ref<InstanceType<typeof ModalComponent>>();
    const formData = reactive({
      title: '',
      description: '',
      releaseAt: undefined,
      dueAt: undefined
    });
    const validator = computed(() => QuestionnaireSchema.validate(formData, { abortEarly: false }));
    const errors = reactive({
      title: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'title')),
      description: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'description')),
      releaseAt: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'releaseAt')),
      dueAt: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'dueAt'))
    });
    const isValid = computed(() => !errors.title && !errors.description && !errors.releaseAt && !errors.dueAt);
    const submited = ref(false);

    const showModal = () => {
      formModal.value?.showModal();
    };

    const hideModal = () => {
      formModal.value?.hideModal();
    };

    const clearSubmit = () => {
      submited.value = false;
    };

    const clearForm = async () => {
      await Promise.all([
        formData.title = '',
        formData.description = '',
        formData.releaseAt = undefined,
        formData.dueAt = undefined
      ]);

      clearSubmit();
    };

    const submitClick = () => {
      submited.value = true;

      if (isValid.value) {
        store.dispatch('questionnaires/create', formData)
          .then(() => {
            toast.success('สร้างแบบสอบถามใหม่เรียบร้อยแล้ว');
            hideModal();
          })
          .catch(() => toast.error('ไม่สามารถสร้างแบบสอบถามได้'));
      }
    };

    return {
      isLoading,
      formModal,
      formData,
      errors,
      submited,
      submitClick,
      showModal,
      hideModal,
      clearForm,
      clearSubmit
    };
  }
});