import { defineComponent, computed } from 'vue';
import { useStore } from 'vuex';
import { useDateFormat } from '../../compositions/dateformat';
import { Questionnaire } from '../../store/models/questionnaire.model';

export default defineComponent({
  name: 'QuestionnaireShow',
  emits: [
    'toggle'
  ],
  setup(_, context) {
    const { toThaiDate } = useDateFormat();

    const store = useStore();
    const questionnaire = computed<Questionnaire>(() => store.getters['questionnaire/selected']);
    const display = computed(() => {
      const today = new Date();

      return {
        title: questionnaire.value.title,
        description: questionnaire.value.description.replace(/(?:\r\n|\r|\n)/g, '<br />'),
        releaseAt: toThaiDate(questionnaire.value.releaseAt, 'long'),
        dueAt: toThaiDate(questionnaire.value.dueAt, 'long'),
        status:  (questionnaire.value.releaseAt <= today)
          ? (questionnaire.value.dueAt < today)
            ? '<span class="text-success">สิ้นสุดแล้ว</span>'
            : '<span class="text-primary">กำลังเปิดรับความคิดเห็น</span>'
          : '<span class="text-dark">ยังไม่เผยแพร่</span>'
      };
    });

    const toggleClick = () => {
      context.emit('toggle');
    };

    return {
      display,
      toggleClick
    };
  }
});
