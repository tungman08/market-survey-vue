import { defineComponent, defineAsyncComponent, ref, computed } from 'vue';

export default defineComponent({
  name: 'QuestionnaireTab',
  components: {
    AsyncQuestionnaireShowComponent: defineAsyncComponent(() => import('../../components/questionnaire-show/QuestionnaireShow.vue')),
    AsyncQuestionnaireEditComponent: defineAsyncComponent(() => import('../../components/questionnaire-edit/QuestionnaireEdit.vue'))
  },
  setup() {
    const isEditMode = ref(false);
    const questionnaireComponent = computed(() => (isEditMode.value)
      ? 'async-questionnaire-edit-component'
      : 'async-questionnaire-show-component');

    const toggleClick = () => {
      isEditMode.value = !isEditMode.value;
    };

    return {
      questionnaireComponent,
      toggleClick
    };
  }
});
