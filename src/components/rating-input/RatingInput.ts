import { defineComponent, ref } from 'vue';
import { QuestionRatingSymbol } from '../../store/models/question.model';

export default defineComponent({
  name: 'RatingInput',
  props: {
    value: {
      type: Number,
      default: () => 0
    },
    level: {
      type: Number,
      default: () => 5
    },
    symbol: {
      type: String as () => QuestionRatingSymbol,
      default: () => 'STAR'
    },
    showLabel: {
      type: Boolean,
      default: () => false
    },
    startLabel: {
      type: String,
      default: () => ''
    },
    endLabel: {
      type: String,
      default: () => ''
    },
    disabled: {
      type: Boolean,
      default: () => false
    }
  },
  emits: [
    'update:value'
  ],
  setup(props, context) {
    const rating = ref(props.value);
    const randomNumber = Math.floor(Math.random() * 1000000);
    const elementId = `rating_input_component_${randomNumber}`;

    const ratingValueChange = () => {
      context.emit('update:value', rating.value);
    };

    return {
      elementId,
      rating,
      ratingValueChange
    };
  }
});
