import { defineComponent, ref, computed } from 'vue';
import { useStore } from 'vuex';
import { Questionnaire } from '../../store/models/questionnaire.model';
import QrcodeVue from 'qrcode.vue';
import ModalComponent from '../modal/Modal.vue';

export default defineComponent({
  name: 'Send',
  components: {
    QrcodeVue,
    ModalComponent
  },
  setup() {
    const sendModal = ref<InstanceType<typeof ModalComponent>>();

    const store = useStore();
    const questionnaire = computed<Questionnaire>(() => store.getters['questionnaire/selected']);
    const surveyUrl = computed<string>(() => `${window.location.origin}/survey/${questionnaire.value?.id}`);

    const showModal = () => {
      sendModal.value?.showModal();
    };

    const hideModal = () => {
      sendModal.value?.hideModal();
    };

    const copyClick = () => {
      console.log('copy');
    };

    const downloadClick = () => {
      console.log('download');
    };

    return {
      sendModal,
      surveyUrl,
      showModal,
      hideModal,
      downloadClick,
      copyClick
    };
  }
});
