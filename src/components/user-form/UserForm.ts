import { defineComponent, ref, reactive, computed } from 'vue';
import { useStore } from 'vuex';
import { useToast } from 'vue-toastification';
import { User } from '../../store/models/user.model';
import { UserSchema } from '../../store/validators/user.validator';
import ModalComponent from '../modal/Modal.vue';

export default defineComponent({
  name: 'UserForm',
  components: {
    ModalComponent
  },
  props: {
    userId: {
      type: String
    },
  },
  setup(props) {
    const store = useStore();
    const toast = useToast();
    const user = computed<User>(() => store.getters['users/single'](props.userId));
    const isLoading = computed<boolean>(() => store.getters['users/status'] === 'loading');
    const isEditMode = computed<boolean>(() => (user.value) ? true : false);
    const userFormModal = ref<InstanceType<typeof ModalComponent>>();

    const formData = reactive({
      username: '',
      password: 'welcome',
      firstName: '',
      lastName: '',
      email: '',
      isAdmin: false,
      active: false,
    });
    const validator = computed(() => UserSchema.validate(formData, { abortEarly: false }));
    const errors = reactive({
      username: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'username')),
      firstName: computed(() =>  validator.value.error?.details.some((detail) => detail.path[0] === 'firstName')),
      lastName: computed(() =>  validator.value.error?.details.some((detail) => detail.path[0] === 'lastName')),
      email: computed(() =>  validator.value.error?.details.some((detail) => detail.path[0] === 'email')),
    });
    const isCreateValid = computed(() => !errors.username && !errors.firstName && !errors.lastName && !errors.email);
    const isUpdateValid = computed(() => !errors.firstName && !errors.lastName && !errors.email);
    const submited = ref(false);

    const userData = computed(() => {
      return (isEditMode.value)
        ? {
          id: user.value.id,
          firstName: formData.firstName,
          lastName: formData.lastName,
          email: formData.email,
          active: formData.active,
          roles: (formData.isAdmin) ? ['USER', 'ADMIN'] : ['USER'] 
        }
        : {
          username: formData.username,
          password: formData.password,
          firstName: formData.firstName,
          lastName: formData.lastName,
          email: formData.email,
          roles: (formData.isAdmin) ? ['USER', 'ADMIN'] : ['USER'] 
        };
    });

    const showModal = () => {
      new Promise<void>((resolve) => {
          if (isEditMode.value) {
            formData.firstName = user.value.firstName;
            formData.lastName = user.value.lastName;
            formData.email = user.value.email;
            formData.isAdmin = user.value.roles.includes('ADMIN');
            formData.active = user.value.active;
          }
          resolve();
        })
        .then(() => userFormModal.value?.showModal())
        .catch((error) => console.log(error));
    };

    const saveClick = () => {
      submited.value = true;

      if (isEditMode.value) {
        updateUser();
      } else {
        createUser();
      }
    };

    const createUser = () => {
      if (isCreateValid.value) {
        store.dispatch('users/create', userData.value)
          .then(() => {
            toast.success('เพิ่มผู้ใช้งานเรียบร้อยแล้ว');
            hideModal();
          })
          .catch(() => toast.error('ไม่สามารถเพิ่มผู้ใช้งานได้'));
      }  
    };

    const updateUser = () => {
      if (isUpdateValid.value) {
        store.dispatch('users/update', userData.value)
          .then(() => {
            toast.success('แก้ไขข้อมูลผู้ใช้งานเรียบร้อยแล้ว');
            hideModal();
          })
          .catch(() => toast.error('ไม่สามารถแก้ไขข้อมูลผู้ใช้งานได้'));
      }
    };

    const hideModal = () => {
      userFormModal.value?.hideModal();
    };

    const clearSubmit = () => {
      submited.value = false;
    };

    const clearForm = async () => {
      await Promise.all([
        formData.username = '',
        formData.firstName = '',
        formData.lastName = '',
        formData.email = '',
        formData.isAdmin = false,
        formData.active = false
      ]);

      clearSubmit();
    };

    return {
      userFormModal,
      isEditMode,
      formData,
      errors,
      submited,
      isLoading,
      showModal,
      hideModal,
      saveClick,
      clearSubmit,
      clearForm
    };
  }
});