import { defineComponent, ref, reactive, computed } from 'vue';
import { useStore } from 'vuex';
import { useToast } from 'vue-toastification';
import { Profile } from '../..//store/models/profile.model';
import { ResetPasswordSchema } from '../../store/validators/user.validator';
import ModalComponent from '../modal/Modal.vue';

export default defineComponent({
  name: 'UserReset',
  components: {
    ModalComponent
  },
  props: {
    userId: {
      type: String,
    },
  },
  setup(props) {
    const store = useStore();
    const toast = useToast();
    const profile = computed<Profile>(() => store.getters['profile/current']);
    const isLoading = computed<boolean>(() => store.getters['users/status'] === 'loading');
    const resetPasswordModal = ref<InstanceType<typeof ModalComponent>>();

    const formData = reactive({
      id: computed(() => props.userId),
      password: 'welcome',
      secretAdmin: computed(() => profile.value?.username),
      secretPassword: '',
    });
    const validator = computed(() => ResetPasswordSchema.validate(formData, { abortEarly: false }));
    const errors = reactive({
      id: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'id')),
      password: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'password')),
      secretPassword: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'secretPassword'))
    });
    const isValid = computed(() => !errors.id && !errors.password && !errors.secretPassword);
    const submited = ref(false);

    const showModal = () => {
      resetPasswordModal.value?.showModal();
    };

    const hideModal = () => {
      resetPasswordModal.value?.hideModal();
    };

    const clearSubmit = () => {
      submited.value = false;
    };

    const clearForm = async () => {
      await Promise.all([
        formData.secretPassword = ''
      ]);

      clearSubmit();
    };
    
    const submitClick = () => {
      submited.value = true;

      if (isValid.value) {
        store.dispatch('users/reset', formData)
          .then(() => toast.success('รีเซ็ตรหัสผ่านเรียบร้อยแล้ว'))
          .catch(() => toast.error('ไม่สามารถรีเซ็ตรหัสผ่านได้'));
      }
    };

    return {
      resetPasswordModal,
      formData,
      errors,
      submited,
      isLoading,
      showModal,
      hideModal,
      clearSubmit,
      clearForm,
      submitClick
    };
  }
});