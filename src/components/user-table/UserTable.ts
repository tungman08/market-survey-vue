import { defineComponent, ref, reactive, computed } from 'vue';
import { useStore } from 'vuex';
import { useSweetAlert } from '../../compositions/sweetalert';
import { useToast } from 'vue-toastification';
import { Profile } from '../../store/models/profile.model';
import { User } from '../../store/models/user.model';
import UserFormComponent from '../user-form/UserForm.vue';
import UserResetComponent from '../user-reset/UserReset.vue';
import PaginateComponent from '../paginate/Paginate.vue';

export default defineComponent({
  name: 'UserTable',
  components: {
    UserFormComponent,
    UserResetComponent,
    PaginateComponent
  },
  setup() {
    const store = useStore();
    const swal = useSweetAlert();
    const toast = useToast();

    const isSecretAdmin = computed<Profile>(() => store.getters['profile/isSecret']);
    const users = computed<User[]>(() => store.getters['users/all']);
    const searchResult = computed<User[]>(() => users.value.filter((user) => toSearch(user)));
    const display = computed<User[]>(() => searchResult.value.filter((_, index) => toDisplay(index)));

    const filter = reactive({
      keyword: '',
      amount: computed(() => searchResult.value.length),
      total: computed(() => users.value.length)
    });

    const page = reactive({
      size: 10,
      current: 1
    });

    const userFormComponent = ref<InstanceType<typeof UserFormComponent>>();
    const userResetComponent = ref<InstanceType<typeof UserResetComponent>>();
    const selectedUserId = ref('');

    const selectedPage = (pageNumber: number) => {
      page.current = pageNumber;
    };

    const newUserClick = async () => {
      await Promise.resolve(selectedUserId.value = '');
      userFormComponent.value?.showModal();
    };

    const editUserClick = async (id: string) => {
      await Promise.resolve(selectedUserId.value = id);
      userFormComponent.value?.showModal();
    };

    const resetPasswordClick = (id: string) => {
      swal.confirm('รีเซ็ตรหัสผ่าน!', 'คุณต้องการรีเซ็ตรหัสผ่านเป็นค่าเริ่มต้นใช่หรือไม่?', 'ใช่ ดำเนินการเดี๋ยวนี้!')
        .then(async (result) => {
          if (result.isConfirmed) {
            await Promise.resolve(selectedUserId.value = id);
            userResetComponent.value?.showModal();
          }
        });
    };

    const deleteUserClick = (id: string) => {
      swal.confirm('ลบผู้ใช้งานระบบ!', 'คุณต้องการลบผู้ใช้งานระบบใช่หรือไม่?', 'ใช่ ลบออกเดี๋ยวนี้!')
        .then((result) => {
          if (result.isConfirmed) {
            store.dispatch('users/remove', id)
              .then(() => toast.success('ลบผู้ใช้งานเรียบร้อยแล้ว'))
              .catch(() => toast.error('ไม่สามารถลบผู้ใช้งานได้'));
          }
        });
    };

    const toSearch = (user: User): User | boolean => {
      const status = (user.active) ? 'ปกติ' : 'ปิดการใช้งาน';
      return (filter.keyword)
        ? user.username.toUpperCase().includes(filter.keyword.toUpperCase())
          || user.firstName.toUpperCase().includes(filter.keyword.toUpperCase())
          || user.lastName.toUpperCase().includes(filter.keyword.toUpperCase())
          || user.email.toUpperCase().includes(filter.keyword.toUpperCase())
          || user.roles.some((role) => role.includes(filter.keyword.toUpperCase()))
          || status.includes(filter.keyword.toUpperCase())
        : user;
    };

    const toDisplay = (index: number) => {
      const start = (page.current - 1) * page.size;
      const end = start + page.size;
      return index >= start && index < end;
    };

    return {
      display,
      filter,
      page,
      userFormComponent,
      userResetComponent,
      selectedUserId,
      isSecretAdmin,
      newUserClick,
      editUserClick,
      resetPasswordClick,
      deleteUserClick,
      selectedPage
    };
  }
});