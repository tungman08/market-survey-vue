import { Token } from '../store/models/auth.model';

interface Authen {
  getExpires: (expiresIn: number) => number,
  modifyToken: (rawToken: Token) => Token,
  getStorageAuthData: () => Token | undefined
}

export const useAuthen = (): Authen => {
  const getExpires = (expiresIn: number): number => {
    const now = new Date();
    return expiresIn - now.getTime();
  };
  
  const modifyToken = (rawToken: Token): Token => {
    const now = new Date();
    const tokenData = {
      accessToken: rawToken.accessToken,
      refreshToken: rawToken.refreshToken,
      expiresIn: now.getTime() + rawToken.expiresIn * 1000,
      refreshExpiresIn: now.getTime() + rawToken.refreshExpiresIn * 1000
    } as Token;
  
    return tokenData;
  };
  
  const getStorageAuthData = (): Token | undefined => {
    const authData = localStorage.getItem('auth');
  
    if (!authData) {
      return undefined;
    }
  
    const token = JSON.parse(authData) as Token;
    return getExpires(token.refreshExpiresIn) > 0
      ? token
      : undefined;
  };

  return {
    getExpires,
    modifyToken,
    getStorageAuthData
  };
};