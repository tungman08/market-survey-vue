interface DateFormat {
  toThaiDateTime: (date: Date, option?: DateTimeOption) => string,
  toThaiDate: (date: Date, option?: DateTimeOption) => string,
  toTime: (date: Date, option?: DateTimeOption) => string
}

export type DateTimeOption = 'full' | 'long' | 'short' | 'narrow';

/* === DateTimeFormatOptions Example ===
interface DateTimeFormatOptions {
  localeMatcher?: 'lookup' | 'best fit';
  weekday?: 'long' | 'short' | 'narrow';
  era?:  'long' | 'short' | 'narrow';
  year?: 'numeric' | '2-digit';
  month?: 'numeric' | '2-digit' | 'long' | 'short' | 'narrow';
  day?: 'numeric' | '2-digit';
  hour?: 'numeric' | '2-digit';
  minute?: 'numeric' | '2-digit';
  second?: 'numeric' | '2-digit';
  timeZoneName?: 'long' | 'short';
  formatMatcher?: 'basic' | 'best fit';
  hour12?: boolean;
  timeZone?: string; // this is more complicated than the others, not sure what I expect here
}
*/

export const useDateFormat = (): DateFormat => {
  const toThaiDateTime = (date: Date, option?: DateTimeOption) => {
    let dateOption: Intl.DateTimeFormatOptions;
    switch (option) {
      case 'full':
        dateOption = { era: 'short', year: 'numeric', month: 'long', weekday: 'long', day: 'numeric', timeZone: 'UTC', hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: false };
        break;
      case 'long':
        dateOption = { year: 'numeric', month: 'long', day: 'numeric', timeZone: 'UTC', hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: false };
        break;
      case 'short':
        dateOption = { year: 'numeric', month: 'short', day: 'numeric', timeZone: 'UTC', hour: 'numeric', minute: 'numeric', hour12: false };
        break;
      case 'narrow': 
        dateOption = { year: 'numeric', month: '2-digit', day: '2-digit', timeZone: 'UTC', hour: 'numeric', minute: 'numeric', hour12: false };
        break;
      default:
        dateOption = { timeZone: 'UTC', hour: 'numeric', minute: 'numeric', hour12: false };
        break;
    }

    return date.toLocaleString('th-TH', dateOption);
  };
  
  const toThaiDate = (date: Date, option?: DateTimeOption) => {
    let dateOption: Intl.DateTimeFormatOptions;
    switch (option) {
      case 'full':
        dateOption = { era: 'short', year: 'numeric', month: 'long', weekday: 'long', day: 'numeric' };
        break;
      case 'long':
        dateOption = { year: 'numeric', month: 'long', day: 'numeric' };
        break;
      case 'short':
        dateOption = { year: 'numeric', month: 'short', day: 'numeric' };
        break;
      case 'narrow': 
        dateOption = { year: 'numeric', month: '2-digit', day: '2-digit' };
        break;
      default:
        dateOption = {};
        break;
    }

    return date.toLocaleDateString('th-TH', dateOption);
  };

  const toTime = (date: Date, option?: DateTimeOption) => {
    let dateOption: Intl.DateTimeFormatOptions;
    switch (option) {
      case 'full':
        dateOption = { timeZone: 'UTC', hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: false };
        break;
      case 'long':
        dateOption = { timeZone: 'UTC', hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: false };
        break;
      case 'short':
        dateOption = { timeZone: 'UTC', hour: 'numeric', minute: 'numeric', hour12: false };
        break;
      case 'narrow': 
        dateOption = { timeZone: 'UTC', hour: 'numeric', minute: 'numeric', hour12: false };
        break;
      default:
        dateOption = { timeZone: 'UTC', hour: 'numeric', minute: 'numeric', hour12: false };
        break;
    }

    return date.toLocaleTimeString('th-TH', dateOption);
  };

  return {
    toThaiDateTime,
    toThaiDate,
    toTime
  };
};
