interface NumberFormat {
  toStringWithCommas: (val: number) => string,
  toThaiText: (val: number) => string
}

export const useNumberFormat = (): NumberFormat => {
  const toStringWithCommas = (val: number) => val.toLocaleString('en-US', { maximumFractionDigits: 3 });

  const toThaiText = (val: number) => {
    const numComposition = val.toString().split('.');
    const integerText = integerToText(numComposition[0]);
    const decimalText = (numComposition.length > 1) ? decimalToText(numComposition[1]) : '';
    
    return (numComposition.length > 1)
      ? `${integerText}จุด${decimalText}`
      : integerText;
  };

  const integerToText = (intString: string) => {
    const reverse = reverseString(intString);
    const strComposition = reverse.match(/.{1,6}/g) as RegExpMatchArray;

    const translated = strComposition.map((str: string, index: number) => {
      const reverseStr = reverseString(str);
      const millian = 'ล้าน'.repeat(index);

      return `${translate(reverseStr)}${millian}`;
    });
      
    return translated.reverse().join('');
  };

  const decimalToText = (decString: string) => {
    const thaiText = ['ศูนย์', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า'];

    return [...decString].map((chr: string) => {
      const num = Number(chr);
      return thaiText[num];
    }).join('');
  };

  const translate = (intString: string) => {
    const position = ['', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน'];
    const thaiText = ['ศูนย์', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า'];
    const reversed = [...reverseString(intString)];
    
    return reversed.map((chr: string, index: number) => {
      const num = Number(chr);
      const translated = (num !== 0 || intString.length === 1)
        ? (num !== 1 || index !== 0 || intString.length === 1)
          ? (num !== 1 || index !== 1)
            ? (num !== 2 || index !== 1)
              ? `${thaiText[num]}${position[index]}`
              : 'ยี่สิบ'
            : 'สิบ'
          : 'เอ็ด'
        : '';
      index++;

      return translated;
    }).reverse().join('');
  };

  const reverseString = (str: string) => [...str].reverse().join('');

  return {
    toStringWithCommas,
    toThaiText
  };
};