import Swal, { SweetAlertResult } from 'sweetalert2';

interface SweetAlert {
  confirm: (title?: string, text?: string, confirmButtonText?: string) => Promise<SweetAlertResult<void>>,
  success: (title?: string, text?: string) => Promise<SweetAlertResult<void>>,
  error: (title?: string, text?: string) => Promise<SweetAlertResult<void>>
}

export const useSweetAlert = (): SweetAlert => {
  const confirm = (title?: string, text?: string, confirmButtonText?: string): Promise<SweetAlertResult<void>> => {
    return Swal.fire<void>({
            title: title || 'Confirm!',
            text: text || 'text',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#ffc107',
            cancelButtonColor: '#6c757d',
            confirmButtonText: confirmButtonText || 'ตกลง',
            cancelButtonText: 'ยกเลิก'
          });
  };

  const success = (title?: string, text?: string): Promise<SweetAlertResult<void>> => {
    return Swal.fire<void>({
            title: title || 'Success!',
            text: text || 'text',
            icon: 'success',
            showCancelButton: false,
            confirmButtonColor: '#ffc107',
            confirmButtonText: 'ตกลง'
          });
  };

  const error = (title?: string, text?: string): Promise<SweetAlertResult<void>> => {
    return Swal.fire<void>({
            title: title || 'Error!',
            text: text || 'text',
            icon: 'error',
            showCancelButton: false,
            confirmButtonColor: '#ffc107',
            confirmButtonText: 'ตกลง'
          });
  };

  return {
    confirm,
    success,
    error
  };
};