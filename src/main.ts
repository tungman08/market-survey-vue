import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Toast from 'vue-toastification';

// add bootstrap and association
import 'bootstrap';
import './assets/scss/bootstrap-custom.scss';

createApp(App)
  .use(store)
  .use(router)
  .use(VueAxios, axios)
  .use(Toast, { position: 'bottom-right' })
  .mount('#app');
