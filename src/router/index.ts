import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import store from '../store';
import LayoutComponent from '../components/layout/Layout.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    meta: { requiresAuth: true },
    component: LayoutComponent,
    children: [
      {
        path: '',
        name: 'HomePage',
        meta: { title: 'หน้าหลัก' },
        component: () => import(/* webpackChunkName: 'home-page' */ '../views/home/Home.vue')
      },
      {
        path: '/questionnaire/:id',
        name: 'QuestionnairePage',
        meta: { title: 'แบบสอบถาม' },
        component: () => import(/* webpackChunkName: 'form-page' */ '../views/questionnaire/Questionnaire.vue')
      },
      {
        path: '/user',
        name: 'UserPage',
        meta: { title: 'จัดการผู้ใช้งาน' },
        component: () => import(/* webpackChunkName: 'user-page' */ '../views/user/User.vue')
      },
    ]
  },
  {
    path: '/auth/login',
    name: 'LoginPage',
    meta: { title: 'เข้าสู่ระบบ' },
    component: () => import(/* webpackChunkName: 'login-page' */ '../views/login/Login.vue')
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFoundPage',
    meta: { title: '404' },
    component: () => import(/* webpackChunkName: 'notfound-page' */ '../views/not-found/NotFound.vue')
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

router.beforeEach((to, _, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  const isAuth = store.getters['auth/isAuth'] as boolean;

  if (requiresAuth && !isAuth) {
    // redirect to login;
    next('/auth/login'); 
  } else {
    const appName = process.env.VUE_APP_NAME || 'VUE';
    document.title = `${appName} - ${to.meta.title}`;
    next(); 
  }
});

export default router;
