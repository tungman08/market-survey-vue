import { AxiosResponse } from 'axios';
import axios from './axios.interceptor';
import { Token } from '../models/auth.model';

export interface AuthService {
  Login: (username: string, password: string) => Promise<AxiosResponse<Token>>;
  Refresh: (accessToken: string, refreshToken: string) => Promise<AxiosResponse<Token>>;
  Logout: () => Promise<AxiosResponse<void>>;
}

const Login = (username: string, password: string): Promise<AxiosResponse<Token>> =>
  axios.Post<Token>('/auth/login', {
    username: username,
    password: password
  });

const Refresh = (accessToken: string, refreshToken: string): Promise<AxiosResponse<Token>> =>
  axios.Post<Token>('/auth/refresh', {
    accessToken: accessToken,
    refreshToken: refreshToken
  });

const Logout = (): Promise<AxiosResponse<void>> =>
  axios.Post<void>('/auth/logout', null);

export default { Login, Refresh, Logout } as AuthService;
