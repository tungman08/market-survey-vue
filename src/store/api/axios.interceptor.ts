import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import store from '..';
import { Token } from '../models/auth.model';

// Initial axios instance
const axiosInstance = axios.create({
  baseURL: process.env.VUE_APP_API_URI || 'http://localhost:3000',
  timeout: 3000
});

// Request interceptor for API calls
axiosInstance.interceptors.request.use(
  (config) => {
    const isAuth = store.getters['auth/isAuth'] as boolean;

    if (isAuth) {
      const token = store.getters['auth/token'] as Token;
      config.headers['Authorization'] = `Bearer ${token.accessToken}`;
    }

    return config;
  },
  (error) => {
    Promise.reject(error);
  });

// Response interceptor for API calls
axiosInstance.interceptors.response.use(
  async (response) => {
    await Promise.resolve(handleDates(response.data));
    return response;
  },
  async (error) => {
    const originalRequest = error.config;

    if (error.response.status === 401 && !originalRequest._retry
      && error.response.data.message === 'Unauthorized, Access Token was expired') {
        
      originalRequest._retry = true;
      const isAuth = store.getters['auth/isAuth'] as boolean;

      if (isAuth) {
        const token = store.getters['auth/token'] as Token;
        
        try {
          await store.dispatch('auth/refresh', token);
          const refreshToken = store.getters['auth/token'] as Token;
          axios.defaults.headers.common['Authorization'] = `Bearer ${refreshToken.accessToken}`;
          originalRequest.data = JSON.parse(originalRequest.data);
          return await axiosInstance(originalRequest);
        } catch (e) {
          return await Promise.reject(error);
        }
      }
    }

    return await Promise.reject(error);
  });

// eslint-disable-next-line
const isIsoDateString = (value: any): boolean => {
  const isoDateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(?:\.\d*)?/;
  return value && typeof value === 'string' && isoDateFormat.test(value);
};

// eslint-disable-next-line
const handleDates = (body: any) => {
  if (body === null || body === undefined || typeof body !== 'object') {
    return body;
  }

  for (const key of Object.keys(body)) {
    const value = body[key];

    if (isIsoDateString(value)) {
      body[key] = new Date(value);
    } else if (typeof value === 'object') {
      handleDates(value);
    }
  }
};

export interface AxiosInstance {
  Get:  <T>(url: string, config?: AxiosRequestConfig | undefined) => Promise<AxiosResponse<T>>;
  Post: <T>(url: string, data: unknown, config?: AxiosRequestConfig | undefined) => Promise<AxiosResponse<T>>;
  Put: <T>(url: string, data: unknown, config?: AxiosRequestConfig | undefined) => Promise<AxiosResponse<T>>;
  Remove: <T>(url: string, config?: AxiosRequestConfig | undefined) => Promise<AxiosResponse<T>>;
}

const Get = <T>(url: string, config?: AxiosRequestConfig | undefined): Promise<AxiosResponse<T>> => {
  return axiosInstance.get(url, config);
};

const Post = <T>(url: string, data: unknown, config?: AxiosRequestConfig | undefined): Promise<AxiosResponse<T>> => {
  return axiosInstance.post(url, data, config);
};

const Put = <T>(url: string, data: unknown, config?: AxiosRequestConfig | undefined): Promise<AxiosResponse<T>> =>  {
  return axiosInstance.put(url, data, config);
};

const Remove = <T>(url: string, config?: AxiosRequestConfig | undefined): Promise<AxiosResponse<T>> =>  {
  return axiosInstance.delete(url, config);
};

export default { Get, Post, Put, Remove } as AxiosInstance;
