import { AxiosResponse } from 'axios';
import axios from './axios.interceptor';
import { Profile } from '../models/profile.model';

export interface ProfileService {
  Get: () => Promise<AxiosResponse<Profile>>;
  Update: (firstName: string, lastName: string, email: string) => Promise<AxiosResponse<Profile>>;
  ChangePassword: (oldPassword: string, password: string) => Promise<AxiosResponse<Profile>>;
}

const Get = (): Promise<AxiosResponse<Profile>> =>
  axios.Get<Profile>('/auth/profile');

const Update = (
  firstName: string,
  lastName: string,
  email: string,
): Promise<AxiosResponse<Profile>> =>
  axios.Post<Profile>('/auth/profile', {
    firstName: firstName,
    lastName: lastName,
    email: email
  });

const ChangePassword = (oldPassword: string, password: string): Promise<AxiosResponse<Profile>> =>
  axios.Post<Profile>('/auth/password', {
    oldPassword: oldPassword,
    password: password
  });

export default { Get, Update, ChangePassword } as ProfileService;