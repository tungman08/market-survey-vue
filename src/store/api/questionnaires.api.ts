import { AxiosResponse } from 'axios';
import axios from './axios.interceptor';
import { Questionnaire } from '../models/questionnaire.model';

export interface QuestionnaireService {
  Fetch: () => Promise<AxiosResponse<Questionnaire[]>>;
  Get: (id: string) => Promise<AxiosResponse<Questionnaire>>;
  Create: (title: string, description: string, releaseAt: Date, dueAt: Date) => Promise<AxiosResponse<Questionnaire>>;
  Update: (id: string, title: string, description: string, releaseAt: Date, dueAt: Date) => Promise<AxiosResponse<Questionnaire>>;
  Remove: (id: string) => Promise<AxiosResponse<void>>;
}

const Fetch = (): Promise<AxiosResponse<Questionnaire[]>> =>
  axios.Get<Questionnaire[]>('/questionnaires');

const Get = (id: string): Promise<AxiosResponse<Questionnaire>> =>
  axios.Get<Questionnaire>(`/questionnaires/${id}`);

const Create = (
  title: string,
  description: string,
  releaseAt: Date,
  dueAt: Date
): Promise<AxiosResponse<Questionnaire>> =>
  axios.Post<Questionnaire>('/questionnaires', {
    title: title,
    description: description,
    releaseAt: releaseAt,
    dueAt: dueAt
  });

const Update = (
  id: string,
  title: string,
  description: string,
  releaseAt: Date,
  dueAt: Date,
): Promise<AxiosResponse<Questionnaire>> =>
  axios.Put<Questionnaire>(`/questionnaires/${id}`, {
    title: title,
    description: description,
    releaseAt: releaseAt,
    dueAt: dueAt
  });

const Remove = (id: string): Promise<AxiosResponse<void>> =>
  axios.Remove<void>(`/questionnaires/${id}`);

export default { Fetch, Get, Create, Update, Remove } as QuestionnaireService;
