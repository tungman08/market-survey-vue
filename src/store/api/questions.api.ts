import { AxiosResponse } from 'axios';
import axios from './axios.interceptor';
import { Question, QuestionType,
  QuestionTextBox, QuestionChoice, QuestionRating, QuestionLikert, QuestionDateTime } from '../models/question.model';

export interface QuestionService {
  Fetch: (questionnaireId: string) => Promise<AxiosResponse<Question[]>>;
  Create: (questionnaireId: string, required: boolean, questionType: QuestionType,
    questionBody: QuestionTextBox | QuestionChoice | QuestionRating | QuestionLikert | QuestionDateTime) => Promise<AxiosResponse<Question>>;
  Update: (id: string, questionnaireId: string, required: boolean, questionType: QuestionType,
    questionBody: QuestionTextBox | QuestionChoice | QuestionRating | QuestionLikert | QuestionDateTime) => Promise<AxiosResponse<Question>>;
  Remove: (questionnaireId: string, id: string) => Promise<AxiosResponse<void>>;
  MoveUp: (questionnaireId: string, id: string) => Promise<AxiosResponse<void>>;
  MoveDown: (questionnaireId: string, id: string) => Promise<AxiosResponse<void>>;
}

const Fetch = (questionnaireId: string): Promise<AxiosResponse<Question[]>> =>
  axios.Get<Question[]>(`/questionnaires/${questionnaireId}/questions`);

const Create = (
  questionnaireId: string,
  required: boolean,
  questionType: QuestionType,
  questionBody: QuestionTextBox | QuestionChoice | QuestionRating | QuestionLikert | QuestionDateTime
): Promise<AxiosResponse<Question>> =>
  axios.Post<Question>(`/questionnaires/${questionnaireId}/questions`, {
    required: required,
    questionType: questionType,
    questionBody: questionBody
  });

const Update = (
  id: string,
  questionnaireId: string,
  required: boolean,
  questionType: QuestionType,
  questionBody: QuestionTextBox | QuestionChoice | QuestionRating | QuestionLikert | QuestionDateTime
): Promise<AxiosResponse<Question>> =>
  axios.Put<Question>(`/questionnaires/${questionnaireId}/questions/${id}`, {
    required: required,
    questionType: questionType,
    questionBody: questionBody
  });

const Remove = (questionnaireId: string, id: string): Promise<AxiosResponse<void>> =>
  axios.Remove<void>(`/questionnaires/${questionnaireId}/questions/${id}`);

const MoveUp = (questionnaireId: string, id: string): Promise<AxiosResponse<void>> =>
  axios.Post<void>(`/questionnaires/${questionnaireId}/questions/${id}/moveup`, null);

const MoveDown = (questionnaireId: string, id: string): Promise<AxiosResponse<void>> =>
  axios.Post<void>(`/questionnaires/${questionnaireId}/questions/${id}/movedown`, null);

export default { Fetch, Create, Update, Remove, MoveUp, MoveDown } as QuestionService;
