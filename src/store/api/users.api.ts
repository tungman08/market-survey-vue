import { AxiosResponse } from 'axios';
import axios from './axios.interceptor';
import { User } from '../models/user.model';

export interface UserService {
  Fetch: () => Promise<AxiosResponse<User[]>>;
  Create: (username: string, password: string, firstName: string, lastName: string, email: string, roles: string[]) => Promise<AxiosResponse<User>>;
  Update: (id: string, firstName: string, lastName: string, email: string, active: boolean, roles: string[]) => Promise<AxiosResponse<User>>;
  Remove: (id: string) => Promise<AxiosResponse<void>>;
  ResetPassword: (id: string, password: string, secretAdmin: string, secretPassword: string) => Promise<AxiosResponse<User>>;
}

const Fetch = (): Promise<AxiosResponse<User[]>> =>
  axios.Get<User[]>('/users');

const Create = (
  username: string,
  password: string,
  firstName: string,
  lastName: string,
  email: string,
  roles: string[]
): Promise<AxiosResponse<User>> =>
  axios.Post<User>('/users', {
    username: username,
    password: password,
    firstName: firstName,
    lastName: lastName,
    email: email,
    roles: roles
  });

const Update = (
  id: string,
  firstName: string,
  lastName: string,
  email: string,
  active: boolean,
  roles: string[]
): Promise<AxiosResponse<User>> =>
  axios.Put<User>(`/users/${id}`, {
    firstName: firstName,
    lastName: lastName,
    email: email,
    active: active,
    roles: roles
  });

const Remove = (id: string): Promise<AxiosResponse<void>> =>
  axios.Remove<void>(`/users/${id}`);

const ResetPassword = (
    id: string,
    password: string,
    secretAdmin: string,
    secretPassword: string
): Promise<AxiosResponse<User>> =>
  axios.Post<User>(`/users/${id}/reset`, {
    password: password,
    secretAdmin: secretAdmin,
    secretPassword: secretPassword
  });

export default { Fetch, Create, Update, Remove, ResetPassword } as UserService;
