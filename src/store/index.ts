import { createStore } from 'vuex';
import authModule from './modules/auth.module';
import profileModule from './modules/profile.module';
import usersModule from './modules/users.module';
import questionnairesModule from './modules/questionnaires.module';
import questionnaireModule from './modules/questionnaire.module';
import questionsModule from './modules/questions.module';

export interface IRootState {
  root: boolean;
  version: string;
}

export default createStore({
  modules: {
    auth: authModule,
    profile: profileModule,
    users: usersModule,
    questionnaires: questionnairesModule,
    questionnaire: questionnaireModule,
    questions: questionsModule
  }
});
