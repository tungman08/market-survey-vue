export interface AuthRequest {
  // eslint-disable-next-line
  [key: string]: any;
}

export interface Token {
  accessToken: string,
  refreshToken: string,
  expiresIn: number,
  refreshExpiresIn: number
}
