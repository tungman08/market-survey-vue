export interface ProfileRequest {
  // eslint-disable-next-line
  [key: string]: any;
}

export interface Profile {
  id?: string,
  username: string,
  firstName: string,
  lastName: string,
  email: string,
  roles: 'ADMIN' | 'USER',
  passwordChanged: boolean
}
