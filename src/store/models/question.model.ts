export interface QuestionRequest {
  // eslint-disable-next-line
  [key: string]: any;
}

export enum QuestionType {
  TextBox = 'TEXTBOX',
  Choice = 'CHOICE',
  Rating = 'RATING',
  Likert = 'LIKERT',
  DateTime = 'DATETIME',
}

export enum QuestionOptionType {
  DropDownList = 'DROPDOWNLIST',
  RadioBox = 'RADIOBOX',
  CheckBox = 'CHECKBOX'
}

export enum QuestionRatingSymbol {
  Star = 'STAR',
  Number = 'NUMBER',
}

export interface QuestionTextBox {
  name: QuestionType.TextBox,
  text: string,
  isTextArea: boolean
}

export interface QuestionChoice {
  name: QuestionType.Choice,
  text: string,
  optionType: QuestionOptionType,
  options: string[],
  hasOther: boolean
}

export interface QuestionRating {
  name: QuestionType.Rating,
  text: string,
  level: number,
  symbol: QuestionRatingSymbol,
  label: { show: boolean, start: string, end: string }
}

export interface QuestionLikert {
  name: QuestionType.Likert,
  text: string,
  isCheckBox: boolean,
  rows: string[],
  columns: string[]
}

export interface QuestionDateTime {
  name: QuestionType.DateTime,
  text: string,
  isTime: boolean
}

export interface Question {
  id: string,
  questionnaire: string,
  required: boolean,
  score: number,
  questionType: QuestionType,
  questionBody: QuestionTextBox | QuestionChoice | QuestionRating | QuestionLikert | QuestionDateTime
  createdAt: Date,
  updatedAt: Date
}
