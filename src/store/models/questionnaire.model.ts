export interface QuestionnaireRequest {
  // eslint-disable-next-line
  [key: string]: any;
}

export interface Questionnaire {
  id: string,
  title: string,
  description: string,
  releaseAt: Date,
  dueAt: Date,
  createdAt: Date,
  updatedAt: Date
}
