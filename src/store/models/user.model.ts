export interface UserRequest {
  // eslint-disable-next-line
  [key: string]: any;
}

export interface User {
  id: string,
  username: string,
  firstName: string,
  lastName: string,
  email: string,
  active: boolean,
  roles: Array<'ADMIN' | 'USER'>,
  passwordChanged: boolean,
  createdAt: Date,
  updatedAt: Date
}
