import { GetterTree, MutationTree, ActionContext, ActionTree, Module } from 'vuex';
import { IRootState } from '..';
import authAPI from '../api/auth.api';
import { Token, AuthRequest } from '../models/auth.model';
import { useAuthen } from '../../compositions/authen';
const { getStorageAuthData, getExpires, modifyToken } = useAuthen();

export interface AuthState {
  token: Token | undefined,
  status: 'idle' | 'loading' | 'failed';
}

// initial state
const authState: AuthState = {
  token: getStorageAuthData(),
  status: 'idle'
};

// getters
export const authGetters: GetterTree<AuthState, IRootState> = {
  isAuth: (state: AuthState) => (state.token && getExpires(state.token.refreshExpiresIn) > 0) ? true : false,
  tokenExpires: (state: AuthState) => (state.token && getExpires(state.token.expiresIn) <= 0) ? true : false,
  token: (state: AuthState) => (state.token)
    ? { 
        accessToken: state.token.accessToken, 
        refreshToken: state.token.refreshToken, 
        expiresIn: getExpires(state.token.expiresIn),
        refreshExpiresIn: getExpires(state.token.refreshExpiresIn)
      }
    : undefined,
  status: (state: AuthState) => state.status
};

// mutations
export const authMutations: MutationTree<AuthState> = {
  token: (state: AuthState, token: Token) => {
    // set localStorage & state
    localStorage.setItem('auth', JSON.stringify(token));
    state.token = token;
  },
  revoke: (state: AuthState) => {
    // remove localStorage & clear state
    localStorage.removeItem('auth');
    state.token = undefined;
  },
  pending: (state: AuthState) => {
    state.status = 'loading';
  },
  fulfilled: (state: AuthState) => {
    state.status = 'idle';
  },
  rejected: (state: AuthState) => {
    state.status = 'failed';
  },
};

// actions
export const authActions: ActionTree<AuthState, IRootState> = {
  login(context: ActionContext<AuthState, IRootState>, payload: AuthRequest) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      authAPI.Login(payload.username, payload.password)
        .then((response) => {
          context.commit('token', modifyToken(response.data));
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  },
  refresh(context: ActionContext<AuthState, IRootState>, payload: AuthRequest) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      authAPI.Refresh(payload.accessToken, payload.refreshToken)
        .then((response) => {
          context.commit('token', modifyToken(response.data));
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  },
  logout(context: ActionContext<AuthState, IRootState>) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      authAPI.Logout()
        .then(() => {
          context.commit('revoke');
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
      });
  }
};

// module
const authModule: Module<AuthState, IRootState> = {
  namespaced: true,
  state: () => (authState),
  getters: authGetters,
  mutations: authMutations,
  actions: authActions
};

export default authModule;
