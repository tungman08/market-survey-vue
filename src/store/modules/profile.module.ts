import { GetterTree, MutationTree, ActionContext, ActionTree, Module } from 'vuex';
import { IRootState } from '..';
import profileAPI from '../api/profile.api';
import { Profile, ProfileRequest } from '../models/profile.model';

export interface ProfileState {
  profile: Profile | undefined,
  status: 'idle' | 'loading' | 'failed';
}

// initial state
const profileState: ProfileState = {
  profile: undefined,
  status: 'idle'
};

// getters
export const profileGetters: GetterTree<ProfileState, IRootState> = {
  isSecret: (state: ProfileState) => (state.profile?.roles.includes('SECRET')) ? true : false,
  isAdmin: (state: ProfileState) => (state.profile?.roles.includes('ADMIN')) ? true : false,
  isUser: (state: ProfileState) => (state.profile?.roles.includes('USER')) ? true : false,
  current: (state: ProfileState) => state.profile,
  status: (state: ProfileState) => state.status
};

// mutations
export const profileMutations: MutationTree<ProfileState> = {
  profile: (state: ProfileState, profile: Profile) => {
    state.profile = profile;
  },
  pending: (state: ProfileState) => {
    state.status = 'loading';
  },
  fulfilled: (state: ProfileState) => {
    state.status = 'idle';
  },
  rejected: (state: ProfileState) => {
    state.status = 'failed';
  },
};

// actions
export const profileActions: ActionTree<ProfileState, IRootState> = {
  get(context: ActionContext<ProfileState, IRootState>) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      profileAPI.Get()
        .then((response) => {
          context.commit('profile', response.data);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  },
  update(context: ActionContext<ProfileState, IRootState>, payload: ProfileRequest) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      profileAPI.Update(
        payload.firstName,
        payload.lastName,
        payload.email
      )
        .then((response) => {
          context.commit('profile', response.data);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  },
  changePassword(context: ActionContext<ProfileState, IRootState>, payload: ProfileRequest) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      profileAPI.ChangePassword(
        payload.oldPassword,
        payload.password
      )
        .then((response) => {
          context.commit('profile', response.data);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  }
};

// module
const profileModule: Module<ProfileState, IRootState> = {
  namespaced: true,
  state: () => (profileState),
  getters: profileGetters,
  mutations: profileMutations,
  actions: profileActions
};

// module
export default profileModule;
