import { GetterTree, MutationTree, ActionContext, ActionTree, Module } from 'vuex';
import { IRootState } from '..';
import questionnaireAPI from '../api/questionnaires.api';
import { Questionnaire, QuestionnaireRequest } from '../models/questionnaire.model';

export interface QuestionnaireState {
  questionnaire?: Questionnaire,
  status: 'idle' | 'loading' | 'failed';
}

// initial state
const questionnaireState: QuestionnaireState = {
  questionnaire: undefined,
  status: 'idle'
};

// getters
export const questionnaireGetters: GetterTree<QuestionnaireState, IRootState> = {
  selected: (state: QuestionnaireState) => state.questionnaire,
  status: (state: QuestionnaireState) => state.status
};

// mutations
export const questionnaireMutations: MutationTree<QuestionnaireState> = {
  fill: (state: QuestionnaireState, questionnaire: Questionnaire) => {
    state.questionnaire = questionnaire;
  },
  flush: (state: QuestionnaireState) => {
    state.questionnaire = undefined;
  },
  pending: (state: QuestionnaireState) => {
    state.status = 'loading';
  },
  fulfilled: (state: QuestionnaireState) => {
    state.status = 'idle';
  },
  rejected: (state: QuestionnaireState) => {
    state.status = 'failed';
  },
};

// actions
export const questionnaireActions: ActionTree<QuestionnaireState, IRootState> = {
  get(context: ActionContext<QuestionnaireState, IRootState>, questionnaireId: string) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      questionnaireAPI.Get(questionnaireId)
        .then((response) => {
          context.commit('fill', response.data);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  },
  create(context: ActionContext<QuestionnaireState, IRootState>, payload: QuestionnaireRequest) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      questionnaireAPI.Create(
        payload.title,
        payload.description,
        payload.releaseAt,
        payload.dueAt
      )
        .then((response) => {
          context.commit('fill', response.data);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  },
  update(context: ActionContext<QuestionnaireState, IRootState>, payload: QuestionnaireRequest) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      questionnaireAPI.Update(
        payload.id,
        payload.title,
        payload.description,
        payload.releaseAt,
        payload.dueAt
      )
        .then((response) => {
          context.commit('fill', response.data);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });  
    });
  },
  remove(context: ActionContext<QuestionnaireState, IRootState>, questionnaireId: string) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      questionnaireAPI.Remove(questionnaireId)
        .then(() => {
          context.commit('flush', questionnaireId);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  }
};

// module
const questionnaireModule: Module<QuestionnaireState, IRootState> = {
  namespaced: true,
  state: () => (questionnaireState),
  getters: questionnaireGetters,
  mutations: questionnaireMutations,
  actions: questionnaireActions
};

// module
export default questionnaireModule;