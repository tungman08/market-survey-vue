import { GetterTree, MutationTree, ActionContext, ActionTree, Module } from 'vuex';
import { IRootState } from '..';
import questionnaireAPI from '../api/questionnaires.api';
import { Questionnaire, QuestionnaireRequest } from '../models/questionnaire.model';
import _ from 'lodash';

export interface QuestionnaireState {
  questionnaires: Array<Questionnaire>,
  status: 'idle' | 'loading' | 'failed';
}

// initial state
const questionnairesState: QuestionnaireState = {
  questionnaires: [],
  status: 'idle'
};

// getters
export const questionnairesGetters: GetterTree<QuestionnaireState, IRootState> = {
  all: (state: QuestionnaireState) => _.orderBy(state.questionnaires, ['updatedAt', 'title'], ['desc', 'asc']),
  single: (state: QuestionnaireState) => (id: string) => state.questionnaires.find((m) => m.id === id),
  status: (state: QuestionnaireState) => state.status
};

// mutations
export const questionnairesMutations: MutationTree<QuestionnaireState> = {
  fetch: (state: QuestionnaireState, questionnaires: Array<Questionnaire>) => {
    state.questionnaires = questionnaires;
  },
  create: (state: QuestionnaireState, questionnaire: Questionnaire) => {
    state.questionnaires.push(questionnaire);
  },
  update: (state: QuestionnaireState, questionnaire: Questionnaire) => {
    const index = state.questionnaires.findIndex((m) => m.id === questionnaire.id);
    state.questionnaires[index] = questionnaire;
  },
  remove: (state: QuestionnaireState, id: string) => {
    const index = state.questionnaires.findIndex((m) => m.id === id);
    state.questionnaires.splice(index, 1);
  },
  pending: (state: QuestionnaireState) => {
    state.status = 'loading';
  },
  fulfilled: (state: QuestionnaireState) => {
    state.status = 'idle';
  },
  rejected: (state: QuestionnaireState) => {
    state.status = 'failed';
  },
};

// actions
export const questionnairesActions: ActionTree<QuestionnaireState, IRootState> = {
  fetch(context: ActionContext<QuestionnaireState, IRootState>) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      questionnaireAPI.Fetch()
        .then((response) => {
          context.commit('fetch', response.data);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  },
  create(context: ActionContext<QuestionnaireState, IRootState>, payload: QuestionnaireRequest) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      questionnaireAPI.Create(
        payload.title,
        payload.description,
        payload.releaseAt,
        payload.dueAt
      )
        .then((response) => {
          context.commit('create', response.data);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  },
  update(context: ActionContext<QuestionnaireState, IRootState>, payload: QuestionnaireRequest) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      questionnaireAPI.Update(
        payload.id,
        payload.title,
        payload.description,
        payload.releaseAt,
        payload.dueAt
      )
        .then((response) => {
          context.commit('update', response.data);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });  
    });
  },
  remove(context: ActionContext<QuestionnaireState, IRootState>, questionnaireId: string) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      questionnaireAPI.Remove(questionnaireId)
        .then(() => {
          context.commit('remove', questionnaireId);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  }
};

// module
const questionnairesModule: Module<QuestionnaireState, IRootState> = {
  namespaced: true,
  state: () => (questionnairesState),
  getters: questionnairesGetters,
  mutations: questionnairesMutations,
  actions: questionnairesActions
};

// module
export default questionnairesModule;
