import { GetterTree, MutationTree, ActionContext, ActionTree, Module } from 'vuex';
import { IRootState } from '..';
import questionAPI from '../api/questions.api';
import { Question, QuestionRequest } from '../models/question.model';
import _ from 'lodash';

export interface QuestionState {
  questions: Array<Question>,
  status: 'idle' | 'loading' | 'failed';
}

// initial state
const questionsState: QuestionState = {
  questions: [],
  status: 'idle'
};

// getters
export const questionsGetters: GetterTree<QuestionState, IRootState> = {
  all: (state: QuestionState) => _.orderBy(state.questions, ['score'], ['asc']),
  single: (state: QuestionState) => (id: string) => state.questions.find((m) => m.id === id),
  status: (state: QuestionState) => state.status
};

// mutations
export const questionsMutations: MutationTree<QuestionState> = {
  fetch: (state: QuestionState, questions: Array<Question>) => {
    state.questions = questions;
  },
  create: (state: QuestionState, question: Question) => {
    state.questions.push(question);
  },
  update: (state: QuestionState, question: Question) => {
    const index = state.questions.findIndex((m) => m.id === question.id);
    state.questions[index] = question;
  },
  remove: (state: QuestionState, id: string) => {
    const index = state.questions.findIndex((m) => m.id === id);
    state.questions.splice(index, 1);
  },
  moveup: (state: QuestionState, id: string) => {
    const index = state.questions.findIndex((m) => m.id === id);
    state.questions[index - 1].score += 1024;
    state.questions[index].score -= 1024;
    const question = state.questions.splice(index, 1)[0];
    state.questions.splice(index - 1, 0, question);
  },
  movedown: (state: QuestionState, id: string) => {
    const index = state.questions.findIndex((m) => m.id === id);
    state.questions[index].score += 1024;
    state.questions[index + 1].score -= 1024;
    const question = state.questions.splice(index, 1)[0];
    state.questions.splice(index + 1, 0, question);   
  },
  pending: (state: QuestionState) => {
    state.status = 'loading';
  },
  fulfilled: (state: QuestionState) => {
    state.status = 'idle';
  },
  rejected: (state: QuestionState) => {
    state.status = 'failed';
  },
};

// actions
export const questionsActions: ActionTree<QuestionState, IRootState> = {
  fetch(context: ActionContext<QuestionState, IRootState>, questionnaireId: string) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      questionAPI.Fetch(questionnaireId)
        .then((response) => {
          context.commit('fetch', response.data);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  },
  create(context: ActionContext<QuestionState, IRootState>, payload: QuestionRequest) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      questionAPI.Create(
        payload.questionnaire,
        payload.required,
        payload.questionType,
        payload.questionBody
      )
        .then((response) => {
          context.commit('create', response.data);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  },
  update(context: ActionContext<QuestionState, IRootState>, payload: QuestionRequest) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      questionAPI.Update(
        payload.id,
        payload.questionnaire,
        payload.required,
        payload.questionType,
        payload.questionBody
      )
        .then((response) => {
          context.commit('update', response.data);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });  
    });
  },
  remove(context: ActionContext<QuestionState, IRootState>, payload: QuestionRequest) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      questionAPI.Remove(payload.questionnaire, payload.id)
        .then(() => {
          context.commit('remove', payload.id);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  },
  moveup(context: ActionContext<QuestionState, IRootState>, payload: QuestionRequest) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      questionAPI.MoveUp(payload.questionnaire, payload.id)
        .then(() => {
          context.commit('moveup', payload.id);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  },
  movedown(context: ActionContext<QuestionState, IRootState>, payload: QuestionRequest) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      questionAPI.MoveDown(payload.questionnaire, payload.id)
        .then(() => {
          context.commit('movedown', payload.id);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  }
};

// module
const questionsModule: Module<QuestionState, IRootState> = {
  namespaced: true,
  state: () => (questionsState),
  getters: questionsGetters,
  mutations: questionsMutations,
  actions: questionsActions
};

// module
export default questionsModule;
