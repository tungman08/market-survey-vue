import { GetterTree, MutationTree, ActionContext, ActionTree, Module } from 'vuex';
import { IRootState } from '..';
import userAPI from '../api/users.api';
import { User, UserRequest } from '../models/user.model';
import _ from 'lodash';

export interface UserState {
  users: Array<User>,
  status: 'idle' | 'loading' | 'failed';
}

// initial state
const usersState: UserState = {
  users: [],
  status: 'idle'
};

// getters
export const usersGetters: GetterTree<UserState, IRootState> = {
  all: (state: UserState) => _.orderBy(state.users, ['firstName', 'lastName'], ['asc', 'asc']),
  single: (state: UserState) => (id: string) => state.users.find((m) => m.id === id),
  status: (state: UserState) => state.status
};

// mutations
export const usersMutations: MutationTree<UserState> = {
  fetch: (state: UserState, users: Array<User>) => {
    state.users = users;
  },
  create: (state: UserState, user: User) => {
    state.users.push(user);
  },
  update: (state: UserState, user: User) => {
    const index = state.users.findIndex((m) => m.id === user.id);
    state.users[index] = user;
  },
  remove: (state: UserState, id: string) => {
    const index = state.users.findIndex((m) => m.id === id);
    state.users.splice(index, 1);
  },
  pending: (state: UserState) => {
    state.status = 'loading';
  },
  fulfilled: (state: UserState) => {
    state.status = 'idle';
  },
  rejected: (state: UserState) => {
    state.status = 'failed';
  },
};

// actions
export const usersActions: ActionTree<UserState, IRootState> = {
  fetch(context: ActionContext<UserState, IRootState>) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      userAPI.Fetch()
        .then((response) => {
          context.commit('fetch', response.data);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  },
  create(context: ActionContext<UserState, IRootState>, payload: UserRequest) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      userAPI.Create(
        payload.username,
        payload.password,
        payload.firstName,
        payload.lastName,
        payload.email,
        payload.roles
      )
        .then((response) => {
          context.commit('create', response.data);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  },
  update(context: ActionContext<UserState, IRootState>, payload: UserRequest) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      userAPI.Update(
        payload.id,
        payload.firstName,
        payload.lastName,
        payload.email,
        payload.active,
        payload.roles
      )
        .then((response) => {
          context.commit('update', response.data);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });  
    });
  },
  remove(context: ActionContext<UserState, IRootState>, userId: string) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      userAPI.Remove(userId)
        .then(() => {
          context.commit('remove', userId);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });
    });
  },
  reset(context: ActionContext<UserState, IRootState>, payload: UserRequest) {
    return new Promise<void>((resolve, reject) => {
      context.commit('pending');
      userAPI.ResetPassword(
        payload.id,
        payload.password,
        payload.secretAdmin,
        payload.secretPassword
      )
        .then((response) => {
          context.commit('update', response.data);
          context.commit('fulfilled');
          resolve();
        })
        .catch(() => {
          context.commit('rejected');
          reject();
        });  
    });
  }
};

// module
const usersModule: Module<UserState, IRootState> = {
  namespaced: true,
  state: () => (usersState),
  getters: usersGetters,
  mutations: usersMutations,
  actions: usersActions
};

// module
export default usersModule;
