import Joi from 'joi';

export const CredentialsSchema = Joi.object({
  username: Joi.string().required(),
  password: Joi.string().required()
});
