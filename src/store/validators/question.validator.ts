import Joi from 'joi';

export const QuestionSchema = Joi.object({
  questionnaire: Joi.string().required(),
  required: Joi.boolean().required(),
  questionType: Joi.string().valid('TEXTBOX', 'CHOICE', 'RATING', 'LIKERT', 'DATETIME').required(),
  questionBody: Joi.alternatives().try(
    Joi.object({ 
      name: Joi.string().valid('TEXTBOX').required(),
      text: Joi.string().required(),
      isTextArea: Joi.boolean().required() 
    }),
    Joi.object({
      name: Joi.string().valid('CHOICE').required(),
      text: Joi.string().required(),
      optionType: Joi.string().valid('DROPDOWNLIST', 'CHECKBOX', 'RADIOBOX').required(),
      options: Joi.array().items(Joi.string()).min(1).required(),
      hasOther: Joi.boolean().required()
    }),
    Joi.object({
      name: Joi.string().valid('RATING').required(),
      text: Joi.string().required(),
      level: Joi.number().required(),
      symbol: Joi.string().valid('STAR', 'NUMBER').required(),
      label: Joi.object({
        show: Joi.boolean().required(),
        start: Joi.string().allow(''),
        end: Joi.string().allow('')
      }).required()
    }),
    Joi.object({
      name: Joi.string().valid('LIKERT').required(),
      text: Joi.string().required(),
      isCheckBox: Joi.boolean().required(),
      rows: Joi.array().items(Joi.string()).min(1).required(),
      columns: Joi.array().items(Joi.string()).min(1).required()
    }),
    Joi.object({
      name: Joi.string().valid('DATETIME').required(),
      text: Joi.string().required(),
      isTime: Joi.boolean().required()
    })
  ).required()
}).assert('.questionBody.name', Joi.ref('questionType'), 'Equal QuestionType');
