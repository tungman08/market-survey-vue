import Joi from 'joi';

export const UserSchema = Joi.object({
  username: Joi.string().required(),
  password: Joi.string().required(),
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  email: Joi.string()
    .pattern(new RegExp(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/))
    .required(),
  roles: Joi.array().items(Joi.string().valid('ADMIN', 'USER')).min(1).required(),
  active: Joi.boolean().required()
});

export const ResetPasswordSchema = Joi.object({
  id: Joi.string().required(),
  password: Joi.string().required(),
  secretAdmin: Joi.string().required(),
  secretPassword: Joi.string().required()
});
