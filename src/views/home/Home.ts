import { defineComponent, onMounted, computed, ref } from 'vue';
import { useRouter } from 'vue-router';
import { useStore } from 'vuex';
import LoadingComponent from '../../components/loading/Loading.vue';
import QuestionnaireListComponent from '../../components/questionnaire-list/QuestionnaireList.vue';
import QuestionnaireNewComponent from '../../components/questionnaire-new/QuestionnaireNew.vue';

export default defineComponent({
  name: 'Home',
  components: {
    LoadingComponent,
    QuestionnaireListComponent,
    QuestionnaireNewComponent
  },
  setup() {
    const router = useRouter();
    const store = useStore();
    const isAdmin = computed<boolean>(() => store.getters['profile/isAdmin']);
    const isUser = computed<boolean>(() => store.getters['profile/isUser']);
    const isLoading = computed<boolean>(() => store.getters['questionnaires/status'] === 'loading');
    const newFormModal = ref<InstanceType<typeof QuestionnaireNewComponent>>();
    const success = ref(false);

    onMounted(() => {
      if (isUser.value) {
        store.dispatch('questionnaires/fetch')
          .then(() => success.value = true)
          .catch(() => console.log('เกิดข้อผิดพลาดในการเรียก questionnaires'));
      }
    });

    const newFormClick = () => {
      newFormModal.value?.showModal();
    };

    const manageUserClick = () => {
      router.push('/user');
    };

    return {
      isLoading,
      isAdmin,
      isUser,
      success,
      newFormModal,
      newFormClick,
      manageUserClick,
    };
  }
});