import { defineComponent, onMounted, ref, reactive, computed } from 'vue';
import { useRouter } from 'vue-router';
import { useStore } from 'vuex';
import { CredentialsSchema } from '../../store/validators/auth.validator';
import { useToast } from 'vue-toastification';
import BackgroundComponent from '../../components/background/Background.vue';

export default defineComponent({
  name: 'Login',
  components: {
    BackgroundComponent
  },
  setup() {
    const router = useRouter();
    const store = useStore();
    const toast = useToast();
    const isAuth = computed<boolean>(() => store.getters['auth/isAuth']);
    const isLoading = computed<boolean>(() => store.getters['auth/status'] === 'loading');

    const credentials = reactive({
      username: '',
      password: ''
    });
    const validator = computed(() => CredentialsSchema.validate(credentials, { abortEarly: false }));
    const errors = reactive({
      username: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'username')),
      password: computed(() => validator.value.error?.details.some((detail) => detail.path[0] === 'password')),
    });
    const isValid = computed(() => !errors.username && !errors.password);
    const submited = ref(false);

    onMounted(() => {
      if (isAuth.value) router.push('/');
    });

    const clearSubmit = () => {
      submited.value = false;
    };

    const submitClick = () => {
      submited.value = true;

      if (isValid.value) {
        store.dispatch('auth/login', credentials)
          .then(() => { if (isAuth.value) router.push('/'); })
          .catch(() => toast.error('ข้อมูลผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง'));
      }
    };

    return {
      credentials,
      errors,
      submited,
      isLoading,
      clearSubmit,
      submitClick
    };
  },
});
