import { defineComponent } from 'vue';
import BackgroundComponent from '../../components/background/Background.vue';

export default defineComponent({
  name: 'NotFound',
  components: {
    BackgroundComponent
  }
});
