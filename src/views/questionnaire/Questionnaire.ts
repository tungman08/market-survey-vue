import { defineComponent, defineAsyncComponent, onMounted, ref, reactive, computed } from 'vue';
import { useRoute } from 'vue-router';
import { useStore } from 'vuex';
import { Questionnaire } from '../../store/models/questionnaire.model';
import PreviewComponent from '../../components/preview/Preview.vue';
import SendComponent from '../../components/send/Send.vue';
import LoadingComponent from '../../components/loading/Loading.vue';

export default defineComponent({
  name: 'Questionnaire',
  components: {
    PreviewComponent,
    SendComponent,
    LoadingComponent,
    AsyncQuestionnaireTabComponent: defineAsyncComponent(() => import('../../components/questionnaire-tab/QuestionnaireTab.vue')),
    AsyncQuestionTabComponent: defineAsyncComponent(() => import('../../components/question-tab/QuestionTab.vue')),
    AsyncResponseTabComponent: defineAsyncComponent(() => import('../../components/response-tab/ResponseTab.vue'))
  },
  setup() {
    const route = useRoute();
    const questionnaireId = route.params.id as string;
    const previewModal = ref<InstanceType<typeof PreviewComponent>>();
    const sendModal = ref<InstanceType<typeof SendComponent>>();

    const store = useStore();
    const questionnaire = ref<Questionnaire>();
    const isLoading = computed<boolean>(() => store.getters['questions/status'] === 'loading');
    const success = ref(false);

    const currentTabComponent = computed(() => tabPanel.tabs[tabPanel.activeTab].name);
    const tabPanel = reactive({
      tabs: [
        { name: 'async-questionnaire-tab-component', text: 'แบบสอบถาม', icon: 'far fa-file me-1' },
        { name: 'async-question-tab-component', text: 'ข้อคำถาม', icon: 'far fa-question-circle me-1' },
        { name: 'async-response-tab-component', text: 'การตอบกลับ', icon: 'far fa-edit me-1' }
      ],
      activeTab: 0
    });

    onMounted(() => {
      store.dispatch('questionnaire/get', questionnaireId)
        .then(() => {
          questionnaire.value = store.getters['questionnaire/selected'];
          store.dispatch('questions/fetch', questionnaireId)
            .then(() => success.value = true)
            .catch(() => console.log('เกิดข้อผิดพลาดในการเรียก questions'));
        })
        .catch(() => console.log('เกิดข้อผิดพลาดในการเรียก questionnaire'));
    });

    const tabClick = (tabIndex: number) => {
      tabPanel.activeTab = tabIndex;
    };

    const previewClick = () => {
      previewModal.value?.showModal();
    };

    const sendClick = () => {
      sendModal.value?.showModal();
    };

    return {
      questionnaire,
      tabPanel,
      currentTabComponent,
      isLoading,
      success,
      previewModal,
      sendModal,
      tabClick,
      previewClick,
      sendClick
    };
  }
});
