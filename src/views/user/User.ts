import { defineComponent, onMounted, ref, computed } from 'vue';
import { useStore } from 'vuex';
import LoadingComponent from '../../components/loading/Loading.vue';
import UserTableComponent from '../../components/user-table/UserTable.vue';

export default defineComponent({
  name: 'User',
  components: {
    LoadingComponent,
    UserTableComponent
  },
  setup() {
    const store = useStore();
    const isLoading = computed<boolean>(() => store.getters['users/status'] === 'loading');
    const success = ref(false);
    
    onMounted(() => {
      store.dispatch('users/fetch')
        .then(() => success.value = true)
        .catch(() => console.log('เกิดข้อผิดพลาดในการเรียก users'));
    });

    return {
      isLoading,
      success
    };
  }
});